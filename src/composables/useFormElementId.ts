import { computed } from 'vue'
import { nanoid } from 'nanoid'

export function useFormElementId(props: { name?: string }) {
  const uniqueId = nanoid()

  // if the input element has a `name` prop that is populated, use this instead of the uniqueId
  const errorsId = computed(() => `error-${props.name || uniqueId}`)
  const hintsId = computed(() => `hints-${props.name || uniqueId}`)
  const legendId = computed(() => `legend-${props.name || uniqueId}`)

  return {
    uniqueId,
    errorsId,
    hintsId,
    legendId,
  }
}

export default useFormElementId
