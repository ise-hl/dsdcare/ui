// eslint-disable-next-line @typescript-eslint/no-var-requires
const dsdcarePreset = require('./tailwind.preset.cjs')

/** @type {import('tailwindcss').Config} */
module.exports = {
  presets: [dsdcarePreset],
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx,md}'],
  theme: {
    extend: {},
  },
  plugins: [],
}
