---
sidebarNavItems:
  [
    { href: '#example-form', label: 'Example form' },
    { href: '#example-input-grid', label: 'Example input grid' },
    {
      href: '#input-fields',
      label: 'Input fields',
      children:
        [
          { href: '#hints', label: 'Hints' },
          { href: '#disabled', label: 'Disabled' },
          { href: '#error-messages', label: 'Error messages' },
          { href: '#icons', label: 'Icons' },
          { href: '#addons', label: 'Addons' },
          { href: '#file', label: 'File' },
        ],
    },
    { href: '#password-input', label: 'Password input' },
    { href: '#textarea', label: 'Textarea' },
    { href: '#select', label: 'Select' },
    { href: '#radio-buttons', label: 'Radio Buttons' },
    { href: '#checkboxes', label: 'Checkboxes' },
    { href: '#highlighted-modifier', label: '`highlighted` Modifier' },
    { href: '#messages', label: 'Messages' },
  ]
---

# Form components

## Example form

An example of a contact form is available on <router-link to="/form-example">form-example</router-link>.

## Example input grid

An example of a `UiInputGrid` is available on <router-link to="/input-grid-example">input-grid-example</router-link>.

## Input fields

<examples-form-input-simple />

<docs-code-snippet>

```html
<template>
  <ui-input label="Your Name" />
</template>
```

</docs-code-snippet>

### Hints

<examples-form-input-hints />

<docs-code-snippet>

```html
<template>
  <ui-input
    label="Postcode (with hints)"
    :hints="['5 characters', 'numbers only!', '']"
    pattern="/^[0-9]{5}$/"
    type="text"
  />
</template>
```

</docs-code-snippet>

### Disabled

<examples-form-input-disabled />

<docs-code-snippet>

```html
<template>
  <ui-input
    label="Email (disabled)"
    hints="format: your-email@your-provider.tld"
    type="email"
    disabled
  />
</template>
```

</docs-code-snippet>

### Error messages

<examples-form-input-error />

<docs-code-snippet>

```html
<template>
  <ui-input
    v-model="name"
    label="Your Name (with error)"
    hints="at least 4 characters"
    :errors="invalid && 'Please enter you name!'"
    required
  >
    <template #iconLeft>
      <pencil-square-icon />
    </template>
  </ui-input>
</template>

<script lang="ts" setup>
  import { computed, ref } from 'vue'
  import { PencilSquareIcon } from '@heroicons/vue/20/solid'
  
  const name = ref('')
  
  const invalid = computed(() => name.value.length < 4)
</script>
```

</docs-code-snippet>

### Icons

<examples-form-input-icon />

<docs-code-snippet>

```html
<template>
  <ui-input
    label="Email (with icon left)"
    hints="format: your-email@your-provider.tld"
    type="email"
  >
    <template #iconLeft>
      <at-symbol-icon />
    </template>
  </ui-input>
</template>

<script lang="ts" setup>
  import { AtSymbolIcon } from '@heroicons/vue/20/solid'
</script>
```

</docs-code-snippet>

### Addons

<examples-form-input-addon />

<docs-code-snippet>

```html
<template>
  <ui-input
    label="How many hours do you sleep per day?"
    hints="a number between 0 and 23"
    type="number"
    min="0"
    max="23"
    addon-label="hours"
  />
</template>
```

</docs-code-snippet>

### File

<examples-form-input-file />

<docs-code-snippet>

```html
<template>
  <form ref="form" class="space-y-4" @submit.prevent="submit">
    <ui-input
      v-model="files"
      label="Your paper"
      type="file"
      accept=".docx,.pdf"
      addon-label="*.docx, *.pdf"
    />
    <ui-button :disabled="!files?.length" :loading="loading">Upload</ui-button>
  </form>
</template>

<script>
  export default {
    data() {
      return {
        files: undefined,
        loading: false,
      }
    },

    methods: {
      async submit() {
        this.loading = true

        const formData = new FormData()
        // files is a FileList object, so we fetch the first file manually
        formData.append('file', this.files[0])

        setTimeout(() => {
          this.loading = false

          // reset the model value
          this.files = undefined

          // reset the native value (file name displayed in the input field)
          this.$refs.form.reset()
        }, 1000)
      },
    },
  }
</script>
```

</docs-code-snippet>

## Password input

<examples-form-password-input />

<docs-code-snippet>

```html
<template>
  <ui-password-input
    v-model="model"
    label="Password"
    :hints="[
      'use the button to toggle the password visibility',
      `don’t use “notSafe”`,
    ]"
    :errors="invalid && 'Your password is: notSafe'"
  />
</template>

<script lang="ts" setup>
  import { computed, ref } from 'vue'

  const model = ref('')
  const invalid = computed(() => model.value === 'notSafe')
</script>
```

</docs-code-snippet>

## Textarea

<examples-form-textarea />

<docs-code-snippet>

```html
<ui-textarea
  label="Your Message"
  :hints="['no reason to be brief', 'the sky is the limit!']"
/>
```

</docs-code-snippet>

<examples-form-textarea-icon />

<docs-code-snippet>

```html
<template>
  <ui-textarea
    label="Your Question (with icon)"
    hints="tell us more about your concern"
  >
    <template #icon>
      <question-mark-circle-icon />
    </template>
  </ui-textarea>
</template>

<script lang="ts" setup>
  import { QuestionMarkCircleIcon } from '@heroicons/vue/20/solid'
</script>
```

</docs-code-snippet>

## Select

<examples-form-select />

<docs-code-snippet>

```html
<template>
  <ui-select label="Subject">
    <option value="account">Account</option>
    <option value="order">Order</option>
    <option value="other">Other</option>
  </ui-select>
</template>
```

</docs-code-snippet>

<examples-form-select-icon />

<docs-code-snippet>

```html
<template>
  <ui-select label="Subject (with icon)">
    <template #icon>
      <tag-icon />
    </template>
    <option value="account">Account</option>
    <option value="order">Order</option>
    <option value="other">Other</option>
  </ui-select>
</template>

<script lang="ts" setup>
  import { TagIcon } from '@heroicons/vue/20/solid'
</script>
```

</docs-code-snippet>

<examples-form-select-multiple />

<docs-code-snippet>

```html
<template>
  <ui-select
    v-model="selected"
    label="Categories (multiple)"
    hints="select one or multiple categories"
    multiple
  >
    <option value="automotive">Automotive</option>
    <option value="electronics">Electronics</option>
    <option value="home-appliances">Home Appliances</option>
    <option value="kitchen">Kitchen</option>
    <option value="pet-supplies">Pet Supplies</option>
  </ui-select>
</template>

<script lang="ts" setup>
  import { ref } from 'vue'

  const selected = ref([])
</script>
```

</docs-code-snippet>

<examples-form-select-multiple-icon />

<docs-code-snippet>

```html
<template>
  <ui-select
    v-model="selected"
    label="Categories (multiple and with icon)"
    hints="select one or multiple categories"
    multiple
  >
    <template #icon>
      <tag-icon />
    </template>
    <option value="automotive">Automotive</option>
    <option value="electronics">Electronics</option>
    <option value="home-appliances">Home Appliances</option>
    <option value="kitchen">Kitchen</option>
    <option value="pet-supplies">Pet Supplies</option>
  </ui-select>
</template>

<script lang="ts" setup>
  import { ref } from 'vue'
  import { TagIcon } from '@heroicons/vue/20/solid'

  const selected = ref([])
</script>
```

</docs-code-snippet>

## Radio buttons

<examples-form-radio-group />

<docs-code-snippet>

```html
<template>
  <ui-radio-group
    v-model="model"
    name="bananas"
    label="Do you like bananas?"
    hints="select one of the following options"
    :options="options"
    :errors="!model && 'Please select an option!'"
    required
  />
</template>

<script lang="ts" setup>
  import { ref } from 'vue'

  const model = ref()
  const options = [
    { id: '1', label: 'Yes' },
    { id: '2', label: 'No' },
    {
      id: '3',
      label: `You always own the option of having no opinion. There is never any need to get worked up or to trouble your soul about things you can't control. These things are not asking to be judged by you. Leave them alone.`,
      disabled: true,
    },
  ]
</script>
```

</docs-code-snippet>

<examples-form-radio-group-buttons />

<docs-code-snippet>

```html
<template>
  <ui-radio-group
    v-model="model"
    name="bananas-buttons"
    label="Do you like bananas? (as buttons)"
    hints="select one of the following options"
    :options="options"
    :errors="!model && 'Please select an option!'"
    buttons
    required
  />
</template>

<script lang="ts" setup>
  import { ref } from 'vue'

  const model = ref()
  const options = [
    { id: '1', label: 'Yes' },
    { id: '2', label: 'No' },
    {
      id: '3',
      label: `You always own the option of having no opinion. There is never any need to get worked up or to trouble your soul about things you can't control. These things are not asking to be judged by you. Leave them alone.`,
      disabled: true,
    },
  ]
</script>
```

</docs-code-snippet>

<examples-form-radio-group-with-reset />

<docs-code-snippet>

```html
<template>
  <ui-radio-group
    v-model="model"
    name="bananas-with-reset"
    label="Do you like bananas? (with reset)"
    :hints="[
      'select one of the following options',
      'reset with the button on the top right (invisible if no option is selected)',
    ]"
    :options="options"
    is-resettable
    reset-button-label="Undo selection"
  />
</template>

<script lang="ts" setup>
  import { ref } from 'vue'

  const model = ref('1')
  const options = [
    { id: '1', label: 'Yes' },
    { id: '2', label: 'No' },
    {
      id: '3',
      label: `You always own the option of having no opinion. There is never any need to get worked up or to trouble your soul about things you can't control. These things are not asking to be judged by you. Leave them alone.`,
      disabled: true,
    },
  ]
</script>
```

</docs-code-snippet>

<examples-form-radio-group-custom />

<docs-code-snippet>

```html
<template>
  <ui-radio-group
    label="Do you like custom layouts? (custom radio elements)"
    hints="select one of the following options"
    :options="options"
    buttons
  >
    <template #default="props">
      <div class="flex flex-row flex-wrap">
        <label
          v-for="option of props.options"
          :key="option.id.toString()"
          class="mr-8"
        >
          <input
            v-model="model"
            type="radio"
            :value="option.id"
            :name="props.nativeName"
          />
          {{ option.label }}
        </label>
      </div>
    </template>
  </ui-radio-group>
</template>

<script lang="ts" setup>
  import { ref } from 'vue'
  import { RadioGroupOption } from '@ise-hl/dsdcare-ui'

  const model = ref()
  const options: RadioGroupOption[] = [
    { id: '1', label: 'Yes' },
    { id: '2', label: 'No' },
  ]
</script>
```

</docs-code-snippet>

## Checkboxes

<examples-form-checkbox-group />

<docs-code-snippet>

```html
<template>
  <ui-checkbox-group
    v-model="model"
    name="favorite-food"
    label="What is your favorite food?"
    hints="select one or multiple of the following options"
    :options="options"
    :errors="!model.length && 'Please select at least one option!'"
    required
  />
</template>

<script lang="ts" setup>
  import { ref } from 'vue'

  const model = ref([])
  const options = [
    { id: '1', label: 'Pizza' },
    { id: '2', label: 'Falafel' },
    { id: '3', label: 'Pizza Hawaii', disabled: true },
    {
      id: '4',
      label:
        'Pasta with lots of vegetables (tomatoes, zucchini, aubergine, bellpepper), garnished with some Parmigiano Reggiano',
    },
  ]
</script>
```

</docs-code-snippet>

<examples-form-checkbox-group-buttons />

<docs-code-snippet>

```html
<template>
  <ui-checkbox-group
    v-model="model"
    name="favorite-food-buttons"
    label="What is your favorite food? (as buttons)"
    hints="select one or multiple of the following options"
    :options="options"
    :errors="!model.length && 'Please select at least one option!'"
    buttons
    required
  />
</template>

<script lang="ts" setup>
  import { ref } from 'vue'

  const model = ref([])
  const options = [
    { id: '1', label: 'Pizza' },
    { id: '2', label: 'Falafel' },
    { id: '3', label: 'Pizza Hawaii', disabled: true },
    {
      id: '4',
      label:
        'Pasta with lots of vegetables (tomatoes, zucchini, aubergine, bellpepper), garnished with some Parmigiano Reggiano',
    },
  ]
</script>
```

</docs-code-snippet>

<examples-form-checkbox-group-custom />

<docs-code-snippet>

```html
<template>
  <ui-checkbox-group
    label="Do you like custom checkboxes? (custom checkboxes)"
    hints="select one or multiple of the following options"
    :options="options"
    buttons
  >
    <template #default="props">
      <div class="flex flex-row flex-wrap">
        <label
          v-for="option of props.options"
          :key="option.id.toString()"
          class="mr-8"
        >
          <input
            v-model="model"
            type="checkbox"
            :value="option.id"
            :name="props.nativeName"
          />
          {{ option.label }}
        </label>
      </div>
    </template>
  </ui-checkbox-group>
</template>

<script lang="ts" setup>
  import { ref } from 'vue'
  import { CheckboxGroupOption } from '@ise-hl/dsdcare-ui'

  const model = ref([])
  const options: CheckboxGroupOption[] = [
    { id: '1', label: 'Yes' },
    { id: '2', label: 'No' },
    { id: '3', label: 'Only every other day' },
  ]
</script>
```

</docs-code-snippet>

## `highlighted` Modifier

All input components illustrated on this page support the `highlighted` modifier.
If this prop is `true`, the 👉 emoji is displayed beneath the component's label to highlight it.
Unless the user has set their browser to prefer reduced motion, it is also animated to draw attention to itself.
For accessibility purposes, `highlightedHint` should be set to provide an textual alternative to the emoji.
The provided text will be displayed as part of the `hints` and provides additional context via `aria-describedby` as such.

<examples-form-checkbox-group-highlighted />

<docs-code-snippet>

```html
<template>
  <ui-checkbox-group
    v-model="model"
    name="favorite-food"
    label="What is your favorite food?"
    hints="select one or multiple of the following options"
    :options="options"
    :highlighted="!model.length"
    :highlighted-hint="!model.length ? 'This question is not answered!' : ''"
  />
</template>

<script lang="ts" setup>
  import { ref } from 'vue'

  const model = ref([])
  const options = [
    { id: '1', label: 'Pizza' },
    { id: '2', label: 'Falafel' },
    { id: '3', label: 'Pizza Hawaii', disabled: true },
    {
      id: '4',
      label:
        'Pasta with lots of vegetables (tomatoes, zucchini, aubergine, bellpepper), garnished with some Parmigiano Reggiano',
    },
  ]
</script>
```

</docs-code-snippet>

## Messages

<examples-form-message />

<docs-code-snippet>

```html
<div class="space-y-6">
  <ui-message> This is a default message box. </ui-message>
  <ui-message black> This is a <strong>black</strong> message box. </ui-message>
  <ui-message gray> This is a <strong>gray</strong> message box. </ui-message>
  <ui-message info> This is an <strong>info</strong> message box. </ui-message>
  <ui-message success>
    This is a <strong>success</strong> message box.
  </ui-message>
  <ui-message warning>
    This is a <strong>warning</strong> message box.
  </ui-message>
  <ui-message danger>
    This is a <strong>danger</strong> message box.
  </ui-message>
</div>
```

</docs-code-snippet>
