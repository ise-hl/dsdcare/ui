import { BuildOptions, defineConfig } from 'vite'
import Dts from 'vite-plugin-dts'
import Vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import Markdown from 'unplugin-vue-markdown/vite'
import MarkdownItAnchor from 'markdown-it-anchor'
import MarkdownItPrism from 'markdown-it-prism'
import { resolve } from 'path'

const dtsSetup = [
  Dts({
    outDir: '@types',
    exclude: ['components.d.ts', 'src/env.d.ts'],
    staticImport: true,
    insertTypesEntry: true,
  }),
]

const libBuild: { build: BuildOptions; publicDir: false } = {
  build: {
    lib: {
      entry: resolve(__dirname, 'src/lib.entrypoint.ts'),
      name: 'DSDCareUI',
      formats: ['es', 'cjs'],
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: [
        'vue',
        'vue-router',
        'nanoid',
        '@vueuse/core',
        '@headlessui/vue',
        '@heroicons/vue',
      ],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue',
          'vue-router': 'vueRouter',
          nanoid: 'nanoid',
          '@vueuse/core': 'VueUseCore',
          '@headlessui/vue': 'HeadlessUiVue',
          '@heroicons/vue': 'HeroiconsVue',
        },
      },
    },
  },
  publicDir: false, // disable use of public dir to exclude favicon from library build
}

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => ({
  define: {
    __LIB_VERSION__: JSON.stringify(process.env.npm_package_version),
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
    },
  },
  plugins: [
    Vue({
      include: [/\.vue$/, /\.md$/],
    }),
    Components({
      directoryAsNamespace: true,
      dts: true,
      extensions: ['vue', 'md'],
      include: [/\.vue($|\?)/, /\.md($|\?)/],
    }),
    createSvgIconsPlugin({
      // Specify the icon folder to be cached
      iconDirs: [resolve(process.cwd(), 'src/icons')],
      // Specify symbolId format
      symbolId: 'icon-[dir]-[name]',
    }),
    Markdown({
      markdownItSetup(md) {
        md.use(MarkdownItAnchor)
        md.use(MarkdownItPrism)
      },
    }),
    ...(command === 'build' && mode === 'library' ? dtsSetup : []),
  ],
  ...(command === 'build' && mode === 'library' ? libBuild : {}),
}))
