/* eslint-disable @typescript-eslint/no-var-requires */
const colors = require('tailwindcss/colors')
const defaultTheme = require('tailwindcss/defaultTheme')
const plugin = require('tailwindcss/plugin')
const forms = require('@tailwindcss/forms')
const typography = require('@tailwindcss/typography')
/* eslint-enable @typescript-eslint/no-var-requires */

const MAX_LINE_LENGTH = '62ch'

module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      blue: colors.sky,
      gray: colors.neutral,
      green: colors.emerald,
      red: colors.rose,
      violet: colors.violet,
      yellow: colors.amber,
    },
    fontFamily: {
      ...defaultTheme.fontFamily,
      sans: ['InterVariable', 'Inter var', 'Inter', ...defaultTheme.fontFamily.sans],
    },
    extend: {
      animation: {
        'spin-irregular': 'spin 1s cubic-bezier(0.75, 0.5, 0.25, 0.5) infinite',
        'bounce-smooth':
          'bounce-smooth 1s cubic-bezier(0.18, 0.89, 0.32, 1.28) infinite',
        'point-right': 'point-right 1.5s ease-in-out infinite',
      },
      keyframes: {
        'bounce-smooth': {
          '0%, 100%': { transform: 'translateY(0)' },
          '50%': { transform: 'translateY(-25%)' },
        },
        'point-right': {
          '0%, 100%': { transform: 'translateX(15%)' },
          '50%': { transform: 'translateX(-15%)' },
        },
      },
      aria: {
        'current-page': 'current="page"',
      },
      maxWidth: {
        prose: MAX_LINE_LENGTH,
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            maxWidth: MAX_LINE_LENGTH,
            hyphens: 'auto',
            color: theme('colors.gray.800'),
            a: {
              color: theme('colors.violet.700'),
              '&:hover': {
                color: theme('colors.violet.900'),
              },
            },
          },
        },
      }),
    },
  },
  plugins: [
    forms,
    typography({
      modifiers: ['lg', 'xl'],
    }),
    plugin(({ addBase, addVariant }) => {
      addBase({
        body: { fontFeatureSettings: '"ss01", "ss03", "ss04"' },
      })
      addVariant(
        'supports-backdrop-blur',
        '@supports (backdrop-filter: blur(0)) or (-webkit-backdrop-filter: blur(0))'
      )
      addVariant(
        'not-disabled',
        '&:not(:disabled)',
      )
    }),
  ],
}
