### STAGE 1: Build static website ###
FROM docker.io/node:lts as build

WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH

RUN corepack enable && corepack prepare pnpm@latest --activate

ARG GITLAB_NPM_TOKEN
COPY .npmrc /usr/src/app/
RUN pnpm config set '//gitlab.com/:_authToken=${GITLAB_NPM_TOKEN}'

COPY pnpm-lock.yaml /usr/src/app/
RUN pnpm fetch

COPY . /usr/src/app/
RUN pnpm install --prefer-offline --frozen-lockfile
RUN pnpm run build

### STAGE 2: Build docker container ###
FROM docker.io/nginx:stable-alpine
COPY ./docker/nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /usr/src/app/dist /usr/share/nginx/html
