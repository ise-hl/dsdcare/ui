---
sidebarNavItems:
  [
    {
      href: '#dialog',
      label: 'Dialog',
      children: [{ href: '#custom-icon', label: 'Custom icon' }],
    },
    {
      href: '#loading',
      label: 'Loading',
      children:
        [
          { href: '#size-modifiers', label: 'Size modifiers' },
          { href: '#overlay', label: 'Overlay' },
          { href: '#with-steps', label: 'With steps' },
          {
            href: '#available-step-statuses',
            label: 'Available step statuses',
          },
          { href: '#overlay-with-steps', label: 'Overlay with steps' },
          { href: '#useloading-composable', label: '`useLoading` composable' },
          { href: '#asyncfn()', label: '`asyncFn()`' },
        ],
    },
  ]
---

# Feedback components

## Dialog

A dialog (modal) component based on [@headlessui/vue](https://headlessui.dev/vue/dialog).

Icons and color styles are applied based on the `Boolean` props `info`, `success`, `warning`, and `danger`.
If none of these properties is set to true, no icon is displayed by default.

Buttons should be placed in the `buttons` slot as illustrated in the example below.

**Note:** Button styles must be set manually, they are not affected by any of the previously mentioned props.

<examples-feedback-dialog />

<docs-code-snippet>

```html
<template>
  <ui-dialog
    :show="show"
    title="Delete Account"
    description="This will permanently delete your account!"
    content="Do you really want to delete your account irreversibly?"
    v-bind="{ info, success, warning, danger }"
    @close="show = false"
  >
    <template #buttons="{ close }">
      <ui-button gray light @click="close()">Cancel</ui-button>
      <ui-button v-bind="{ success, warning, danger }" @click="close()">
        Confirm
      </ui-button>
    </template>
  </ui-dialog>

  <div class="flex w-full items-end gap-8">
    <ui-select
      v-model="chosenOptions"
      label="Dialog type"
      hints="Sets default icon and its color."
      :options="typeOptions"
    />
    <ui-button @click="show = true">Show Dialog</ui-button>
  </div>
</template>

<script lang="ts" setup>
  import { computed, ref } from 'vue'

  const typeOptions = ['default', 'info', 'success', 'warning', 'danger']

  const show = ref(false)
  const chosenOptions = ref<string[]>([])

  const info = computed(() => chosenOptions.value.includes('info'))
  const success = computed(() => chosenOptions.value.includes('success'))
  const warning = computed(() => chosenOptions.value.includes('warning'))
  const danger = computed(() => chosenOptions.value.includes('danger'))
</script>
```

</docs-code-snippet>

### Custom icon

Instead of the icons applied by default when using one of the `Boolean` type props, you may also use a custom icon via the `icon` slot. By default, the colors of the chosen style are applied to your custom SVG icon.

<examples-feedback-dialog-custom-icon />

<docs-code-snippet>

```html
<template>
  <ui-dialog
    :show="show"
    title="Having fun yet?"
    content="Are you having fun yet?"
    @close="show = false"
  >
    <template #icon>
      <face-smile-icon class="stroke-2" />
    </template>
    <template #buttons="{ close }">
      <ui-button @click="close()">Of course!</ui-button>
    </template>
  </ui-dialog>

  <ui-button @click="show = true">Show Dialog</ui-button>
</template>

<script lang="ts" setup>
  import { ref } from 'vue'
  import { FaceSmileIcon } from '@heroicons/vue/24/outline'

  const show = ref(false)
</script>
```

</docs-code-snippet>

## Loading

Display a simple spinner to indicate a loading state.

**Note:** The required prop `label` serves as a description for screenreaders.

<examples-feedback-loading />
<docs-code-snippet>

```html
<ui-loading label="Loading, please wait!" />
```

</docs-code-snippet>

### Size modifiers

Use one of the boolean props `sm`, `md`, `lg`, or `xl` to adjust the size of the spinner.

<examples-feedback-loading-sizes />
<docs-code-snippet>

```html
<template>
  <div class="loading-grid">
    <code>sm</code>
    <ui-loading label="Loading, please wait!" sm />

    <code>default</code>
    <ui-loading label="Loading, please wait!" />

    <code>md</code>
    <ui-loading label="Loading, please wait!" md />

    <code>lg</code>
    <ui-loading label="Loading, please wait!" lg />

    <code>xl</code>
    <ui-loading label="Loading, please wait!" xl />
  </div>
</template>

<!-- custom styles for layout purposes only -->
<style lang="postcss" scoped>
  .loading-grid {
    margin: 0 auto;
    display: flex;
    flex-direction: column;
  }

  code {
    text-align: center;
  }

  .ui-loading {
    margin-bottom: 1.5rem;
  }

  @screen lg {
    .loading-grid {
      display: grid;
      grid-template-columns: repeat(5, minmax(min-content, 1fr));
      grid-template-rows: repeat(2, min-content);
      grid-auto-flow: column;
    }
  }
</style>
```

</docs-code-snippet>

### Overlay

Display the UiLoading component as a fullscreen overlay via the boolean prop `overlay`.

**Note:** This example uses the boolean prop `withScrollLock` which enables [`useScrollLock`](https://vueuse.org/core/useScrollLock/) on the page's `body` element.

<examples-feedback-loading-overlay />

<docs-code-snippet>

```html
<template>
  <ui-button @click="show">Toggle loading overlay</ui-button>
  <ui-transition-fade>
    <ui-loading
      :is-active="isActive"
      label="Loading, please wait!"
      overlay
      with-scroll-lock
      @click="isActive = false"
    />
  </ui-transition-fade>
</template>

<script lang="ts" setup>
  import { ref } from 'vue'

  const isActive = ref(false)

  const show = () => {
    isActive.value = true

    setTimeout(() => (isActive.value = false), 3000)
  }
</script>
```

</docs-code-snippet>

### With steps

Display `LoadingSteps` along with the UiLoading component.

<examples-feedback-loading-with-steps />

<docs-code-snippet>

```html
<template>
  <ui-loading label="Loading, please wait!" :steps="steps" lg />
</template>

<script lang="ts" setup>
  import { LoadingStepStatus } from '@ise-hl/dsdcare-ui'

  const steps = [
    { label: 'Fetching episodes', status: LoadingStepStatus.Done },
    {
      label: 'Determining latest valid episode',
      status: LoadingStepStatus.Done,
    },
    { label: 'Fetching form states', status: LoadingStepStatus.Running },
    {
      label: 'Merging episode forms with Journey forms',
      status: LoadingStepStatus.Pending,
    },
    { label: 'Removing completed forms', status: LoadingStepStatus.Pending },
  ]
</script>
```

</docs-code-snippet>

### Available step statuses

<examples-feedback-loading-step-status-types />

<docs-code-snippet>

```html
<template>
  <ui-loading label="Loading, please wait!" :steps="steps" lg />
</template>

<script lang="ts" setup>
  import { LoadingStepStatus } from '@ise-hl/dsdcare-ui'

  const steps = [
    { label: 'Done step', status: LoadingStepStatus.Done },
    { label: 'Skipped step', status: LoadingStepStatus.Skipped },
    { label: 'Step with error', status: LoadingStepStatus.Error },
    { label: 'Running step', status: LoadingStepStatus.Running },
    { label: 'Pending step', status: LoadingStepStatus.Pending },
    { label: 'Cancelled step', status: LoadingStepStatus.Cancelled },
    { label: 'Step without status' },
  ]
</script>
```

</docs-code-snippet>

### Overlay with steps

Display the UiLoading component as a fullscreen overlay and use `steps` with `Running` and `Pending` statuses to visualize a multi-step action.

**Note:** This example uses [`useIntervalFn`](https://vueuse.org/shared/useIntervalFn/) from [VueUse](https://vueuse.org/) to add the next step and update the status of the previous step once per second.

<examples-feedback-loading-overlay-with-steps />

<docs-code-snippet>

```html
<template>
  <ui-button @click="toggleLoading">Toggle loading overlay</ui-button>
  <ui-transition-fade>
    <ui-loading
      :is-active="isActive"
      label="Loading, please wait!"
      :steps="loadingSteps"
      overlay
      with-scroll-lock
      lg
      @click="toggleLoading"
    />
  </ui-transition-fade>
</template>

<script lang="ts" setup>
  import { ref } from 'vue'
  import { useIntervalFn } from '@vueuse/core'
  import { LoadingStep, LoadingStepStatus } from '@ise-hl/dsdcare-ui'

  const isActive = ref(false)
  const loadingSteps = ref<LoadingStep[]>([])

  const steps = [
    'Fetching episodes',
    'Determining latest valid episode',
    'Fetching form states',
    'Merging episode forms with Journey forms',
    'Removing completed forms',
  ]

  const { pause, resume } = useIntervalFn(
    () => {
      const index = loadingSteps.value.length - 1

      if (index >= 0) {
        loadingSteps.value[index].status = LoadingStepStatus.Done
      }

      if (steps[index + 1]) {
        loadingSteps.value.push({
          label: steps[index + 1],
          status: LoadingStepStatus.Running,
        })
      } else {
        toggleLoading()
      }
    },
    1000,
    { immediate: false },
  )

  const toggleLoading = () => {
    isActive.value = !isActive.value

    if (!isActive.value) {
      return pause()
    }

    loadingSteps.value = [
      {
        label: steps[0],
        status: LoadingStepStatus.Running,
      },
    ]

    resume()
  }
</script>
```

</docs-code-snippet>

### `useLoading` composable

The `useLoading` composable can be used to control a `UiLoading` component defined on the app root.
It serves as a shared service that manages the state of the component.

#### App component

```html
<template>
  <ui-page>
      <!-- ... -->
  </ui-page>
  <ui-loading v-if="isActive" :steps="steps">
</template>

<script lang="ts" setup>
import { useLoading } from '@ise-hl/dsdcare-ui'

const { isActive, steps } = useLoading()
</script>
```

#### Child component

```html
<template>
  <ui-button @click="$loading.show">Show loading</ui-button>
  <ui-button @click="$loading.hide">Hide loading</ui-button>
</template>

<script lang="ts" setup>
  import { useLoading } from '@ise-hl/dsdcare-ui'

  const { $loading } = useLoading()
</script>
```

#### Example with local `UiLoading` component

<examples-feedback-loading-with-service />

<docs-code-snippet>

```html
<template>
  <div class="mb-8 flex flex-wrap gap-4">
    <ui-button @click="$loading.show">Show loading</ui-button>
    <ui-button :disabled="!isActive" @click="$loading.hide"
      >Hide loading</ui-button
    >
    <ui-button
      :disabled="!isActive || steps.length === exampleSteps.length"
      @click="$loading.addStep(exampleSteps[steps.length])"
    >
      Add step
    </ui-button>
    <ui-button
      :disabled="!isActive || !steps.length"
      @click="
        $loading.updateStep(lastStepId, {
          status: LoadingStepStatus.Skipped,
        })
      "
    >
      Update last step
    </ui-button>
    <ui-button
      :disabled="!isActive || !steps.length"
      @click="$loading.removeStep(lastStepId)"
    >
      Remove last step
    </ui-button>
    <ui-button
      :disabled="!isActive || !steps.length"
      @click="$loading.clearSteps()"
    >
      Clear steps
    </ui-button>
  </div>
  <ui-loading
    :is-active="isActive"
    label="Loading, please wait!"
    :steps="steps"
  />
</template>

<script lang="ts" setup>
  import { computed } from 'vue'
  import { LoadingStepStatus, useLoading } from '@ise-hl/dsdcare-ui'

  const { isActive, steps, $loading } = useLoading()

  const lastStepId = computed(() => steps.value[steps.value.length - 1].id)

  const exampleSteps = [
    { id: 0, label: 'Fetching episodes', status: LoadingStepStatus.Done },
    {
      id: 1,
      label: 'Determining latest valid episode',
      status: LoadingStepStatus.Done,
    },
    { id: 2, label: 'Fetching form states', status: LoadingStepStatus.Running },
    {
      id: 3,
      label: 'Merging with Journey forms',
      status: LoadingStepStatus.Pending,
    },
    {
      id: 4,
      label: 'Removing completed forms',
      status: LoadingStepStatus.Pending,
    },
  ]
</script>
```

</docs-code-snippet>

### `asyncFn()`

When running an async function before and after which you want to update the `status` of a `LoadingStep`, you can use `$loading.asyncFn()`. This method automatically updates the status of a `LoadingStep` before and after execution of the function -- or in case an error occurs during function execution.

For UX purposes, a minimum duration can be specified via `minDuration`. This allows keeping the status visible if the function is executed quickly. This behavior is demonstrated in the following example, in which `fn` is executed in `500ms` but the `LoadingStep` status is updated with the `Done` status after `minDuration` of `2000ms`

<examples-feedback-loading-async-fn />

<docs-code-snippet>

```html
<template>
  <div class="mb-8 flex flex-wrap gap-4">
    <ui-button @click="run()">Run async fn</ui-button>
    <ui-button @click="run({ error: true })">Run async fn with error</ui-button>
  </div>
  <ui-loading label="Loading, please wait!" :steps="steps" />
</template>

<script lang="ts" setup>
  import { LoadingStepStatus, useLoading } from '@ise-hl/dsdcare-ui'

  const { steps, $loading } = useLoading()

  const fn = ({ error = false } = {}) =>
    new Promise<void>((resolve, reject) =>
      setTimeout(() => {
        if (error) {
          return reject(new Error())
        }

        $loading.addStep(
          {
            id: 'fn-done',
            label: 'Fn done',
            status: LoadingStepStatus.Done,
          },
          { insertBeforeId: 'async-fn' }, // insert before 'async-fn' step
        )
        resolve()
      }, 500),
    )

  const run = async ({ error = false } = {}) => {
    $loading.clearSteps()

    await $loading.asyncFn({
      step: {
        label: 'Async fn with min duration of 2s',
        id: 'async-fn',
      },
      fn: fn({ error }),

      minDuration: 2000,
    })
  }
</script>
```

</docs-code-snippet>
