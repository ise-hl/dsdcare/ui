---
sidebarNavItems:
  [
    {
      href: '#page',
      label: 'Page',
      children:
        [
          { href: '#slots', label: 'Slots' },
          { href: '#props', label: 'Props' },
        ],
    },
    { href: '#navbar', label: 'Navbar' },
    { href: '#breadcrumbs', label: 'Breadcrumbs' },
  ]
---

# Layout components

## Page

The `UiPage` component serves as a common default layout for different purposes.

### Slots

#### `header` slot

Most commonly, you would use the `header` slot for the `UiNavbar` of a page.

<docs-code-snippet>

```html
<ui-page>
  <template #header>
    <ui-navbar v-bind="navbarOptions" />
  </template>
</ui-page>
```

</docs-code-snippet>

#### `default` slot

This slot should be used for the main page content. It is wrapped by a `UiBox` and positioned within a `UiContainer`.

By default, the `prose` classes of the `@tailwindcss/typography` classes are applied to the slot's wrapper. You can disable this behaviour by setting the `noProse` prop to `true`.

<docs-code-snippet>

```html
<ui-page :has-custom-content="false" :no-prose="false">
  Default slot content
</ui-page>
```

</docs-code-snippet>

<ui-call-out info>

**Note**: If you want to use a custom layout for your content, set the `hasCustomContent` prop to `true`; In this case, the `UiContainer` and`UiBox` are not used as wrappers. Additionally, the `prose` classes will not be applied.

</ui-call-out>

#### `sidebar` Slot

The `sidebar` slot allows you to display additional content beneath (or before on narrow screens) the main content. As such, it is used within the `UiContainer` and wrapped by an `UiBox` just like the `default` slot.
By default, the slot's content is wrapped by an `aside` element.

<docs-code-snippet>

```html
<ui-page has-sidebar>
  <template #sidebar>
    <ui-sidebar-nav v-bind="sidebarOptions" />
  </template>

  Main Content
</ui-page>
```

</docs-code-snippet>

<ui-call-out info>

**Note:** In order to display the `sidebar` slot's content, you must set the `hasSidebar` prop to `true`! Also, the slot is not available if `hasCustomContent` is set to `true`!

</ui-call-out>

#### `preMain` slot

If you want to display content before the `default` slot but within the `UiBox` that wraps the `default` slot, you can use the `preMain` slot. This can be useful if you want to control breadcrumbs of the page from the app root instead of the current page.

<docs-code-snippet>

```html
<ui-page has-sidebar>
  <template #preMain>
    <ui-breadcrumbs v-bind="breadcrumbOptions" />
  </template>

  <router-view />
</ui-page>
```

</docs-code-snippet>

#### `footer` slot

For any content following the main content, you can use the `header` slot.

<docs-code-snippet>

```html
<ui-page>
  Main content

  <template #footer>
    <footer>...</footer>
  </template>
</ui-page>
```

</docs-code-snippet>

### Props

The following boolean props are available:

```js
{
  noProse: false,
  hasSidebar: false,
  hasCustomContent: false
}
```

## Navbar

<div class="not-prose">
  <examples-layout-navbar />
</div>

<docs-code-snippet>

```html
<ui-navbar
  :brand="{ name: 'DSDCare', to: '/' }"
  :navbar-items="[
    { label: 'Home', to: '/' },
    { label: 'Layout', to: '/layout' },
    { label: 'Login', tag: 'button' },
  ]"
  :sticky="false"
/>
```

</docs-code-snippet>

## Breadcrumbs

<div class="not-prose">
<examples-layout-breadcrumbs />
</div>

<docs-code-snippet>

```html
<ui-breadcrumbs
  id="breadcrumbs"
  :items="[
    { label: 'Home', to: '/' },
    { label: 'Layout', to: '#' },
    { label: 'Breadcrumbs' },
  ]"
/>
```

</docs-code-snippet>
