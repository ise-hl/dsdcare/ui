/**
 * Shared Types and Interfaces.
 */

/**
 * Define properties K to be optional on type T.
 *
 * @example
 * ```
 * // The object assigned to test must include all (by default non-optional) properties of `MyType`
 * // except for `prop1` and `prop2` which are optional.
 * const test: Optional<MyType, 'prop1' | 'prop2'> = ...
 * ```
 */
export type Optional<T, K extends keyof T> = Partial<T> & Omit<T, K>
