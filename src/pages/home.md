---
sidebarNavItems:
  [
    { href: '#setup', label: 'Setup' },
    { href: '#tailwind-setup', label: 'Tailwind setup' },
    { href: '#purgecss-setup', label: 'PurgeCSS setup' },
    { href: '#typography', label: 'Typography' },
    { href: '#svg-icons', label: 'SVG icons' },
    { href: '#component-usage', label: 'Component Usage' },
  ]
---

# DSDCare UI

<ui-call-out>

**Compatibility with Vue**

- v1.0.0 - v1.1.1 of DSDCare UI require at least Vue 3.2.
- Starting with v1.2.0, Vue 3.3 or greater is required.

</ui-call-out>

## Setup

1. Setup access to the registry:

   ```shell
   $ echo @ise-hl:registry=https://gitlab.com/api/v4/packages/npm >> .npmrc
   ```

2. Install via `pnpm` (or any other node package manager of your choice):

   ```shell
   $ pnpm add @ise-hl/dsdcare-ui
   ```

## Tailwind setup

The components are tailored for the tailwind setup of this project which relies on the preset bundled with the module:

`tailwind.preset.cjs`

Accordingly, it is recommended to use the same settings in the `tailwind.config.cjs` of your own project.
You can use the preset as follows:

```js
const dsdcarePreset = require('@ise-hl/dsdcare-ui/tailwind.preset')

module.exports = {
  presets: [dsdcarePreset],
  /* ... */
}
```

Furthermore, you have to import [Tailwindcss](https://tailwindcss.com) in your app:

1. Create a stylesheet that declares the `@tailwind` directives.

   ```css
   /* src/assets/index.css */
   @tailwind base;
   @tailwind components;
   @tailwind utilities;
   ```

2. Import the previously created stylesheet in your `main` script.

   ```typescript
   /* src/main.{js,ts} */
   import { createApp } from 'vue'
   import App from '@/App.vue'

   import './assets/index.css' // 👈

   createApp(App).mount('#app')
   ```

### PurgeCSS setup

The components of this library must be explicitly added to the `content` property of the `tailwind.config.js`:

```js
{
  /* ... */
  content: [
    /* typical vite setup: */
   './index.html',
   './src/**/*.{vue,js,ts,jsx,tsx}',
    /* include DSDCare UI components: */
    'node_modules/@ise-hl/dsdcare-ui/**/*.js',
  ],
  /* ... */
}
```

## Typography

By default, the font [Inter](https://rsms.me/inter) by Rasmus Andersson is used.
The easiest way to use it is to add it to the `head` section of your `index.html`

```html
<!-- index.html -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- ... -->
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
  </head>
  <body>
    <!-- ... -->
  </body>
</html>
```

Alternatively, if you prefer self-hosted fonts, you can install [`inter-ui`](https://www.npmjs.com/package/inter-ui) and import it in your `main` script:

```typescript
/* src/main.{js,ts} */
import { createApp } from 'vue'
import App from '@/App.vue'

import 'inter-ui/inter.css' // 👈
import './assets/index.css'

createApp(App).mount('#app')
```

## SVG icons

In order to use SVG icons (e.g. the Navbar allows icon usage for its items by passing the icon's name as part of the `navbarItems` prop), this implementation relies on [`vite-plugin-svg-icons`](https://github.com/anncwb/vite-plugin-svg-icons).

In order to use it, make sure to add it to your `vite.config.{js,ts}`:

```typescript
/* vite.config.ts */
import { defineConfig } from 'vite'
import Vue from '@vitejs/plugin-vue'
import ViteSvgIcons from 'vite-plugin-svg-icons'
import { resolve } from 'path'

export default defineConfig({
  plugins: [
    Vue(),
    ViteSvgIcons({
      // Specify the icon folder to be cached
      iconDirs: [resolve(process.cwd(), 'src/icons')],
      // Specify symbolId format
      symbolId: 'icon-[dir]-[name]',
    }),
  ],
})
```

If setup as above, icons yout want to use must be located in the directory `/src/icons/`.
For example, if you want to use the icon named `arrow-left`, the file `arrow-left.svg` must be present in this directory.

Alternatively, most components provide slots which allow inline definition of SVG icons.

## Component usage

Import components as required, e.g.

```html
<template>
  <div>
    <ui-button>Click me!</ui-button>
  </div>
</template>
<script lang="ts" setup>
  import { UiButton } from '@ise-hl/dsdcare-ui'
</script>
```

## License

[BSD-3-Clause](https://opensource.org/license/bsd-3-clause/)

Copyright (c) 2023, Institute for Social Medicine and Epidemiology, University of Lübeck
