module.exports = {
  extends: ['stylelint-config-recommended-vue'],
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
  overrides: [
    {
      files: ['**/*.css'],
      rules: {
        'at-rule-no-unknown': [
          true,
          {
            ignoreAtRules: ['tailwind'],
          },
        ],
      },
    },
    {
      files: ['**/*.vue'],
      customSyntax: 'postcss-html',
      rules: {
        'at-rule-no-unknown': [
          true,
          {
            ignoreAtRules: ['extends', 'tailwind', 'screen', 'layer'],
          },
        ],
        'function-no-unknown': [true, { ignoreFunctions: ['theme', 'v-bind'] }],
      },
    },
  ],
}
