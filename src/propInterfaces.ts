import type { RouteLocationRaw } from 'vue-router'

/**
 * Interfaces which are used as component `PropTypes`.
 */

import { LoadingStepStatus } from './enums'

export interface BreadcrumbItem {
  to?: RouteLocationRaw
  label: string
}

/**
 * Either `label` or `labelledById` is required.
 * If `labelledById` is provided, `label` will not be displayed.
 */
export interface CheckboxGroupOption {
  id: string | number | boolean
  label?: string | number | boolean
  labelledById?: string
  disabled?: boolean
}

export interface LanguageSelectLocaleItem {
  code: string // The language code, e.g an ISO 639-1 code.
  name: string // The language name.
  to: RouteLocationRaw // The RouterLink path.
}

export interface LanguageSelectOptions {
  label: string // Select element label (only visible to screen readers).
  locales: Array<LanguageSelectLocaleItem> // The available languages.
  selectedLocale: string // The language code of a `LocaleItem` that represents the currently used language.
  supportNoScript: boolean // Whether to support clients with JavaScript disabled.
}

export interface LoadingStep {
  id: number | string
  label: string
  status?: LoadingStepStatus
}

export interface NavbarItemProps {
  label: string // Item label text.
  tag?: string // Component or HTML tag to use, e.g. `a` for external links or `button`.
  to?: RouteLocationRaw // If the item is a `router-link` (`tag="router-link"`).
  matchNonExactRoute?: boolean // If the item is a `router-link`, this will apply `active` styling even for non exact route matches, i.e. when a child route is active.
  href?: string // The URL if the item is an anchor element (`tag="a"`).
  external?: boolean // Whether to apply default settings to the `rel` and `target` attributes and display an icon indicating the link is external.
  onClick?: () => void // `@click` handler if `tag="button"`.
  iconLeft?: string // Name of the left icon.
  iconRight?: string // Name of the right icon. Note: Overwrites the `external` icon if present.
}

export interface NavbarBrand {
  name: string
  to: RouteLocationRaw
}

/**
 * Either `label` or `labelledById` is required.
 * If `labelledById` is provided, `label` will not be displayed.
 */
export interface RadioGroupOption {
  id: string | number | boolean
  label?: string | number | boolean
  labelledById?: string
  disabled?: boolean
}

export interface SelectOption {
  id?: string | number | boolean
  label?: string | number | boolean
  value?: string | number | boolean
}

export interface SidebarNavItemData {
  label: string
  href?: string
  to?: RouteLocationRaw
  /** If the item is a `router-link`, this will apply `active` styling even for non exact route matches, i.e. when a child route is active. */
  matchNonExactRoute?: boolean
  tag?: string
  children?: Array<SidebarNavItemData>
  hasOrderedChildren?: boolean
  iconLeft?: string
  iconRight?: string
  /** Custom classes applied to the root element (`li`). */
  customClass?: string
  /** Custom classes applied to the `component` (`tag`) element. */
  customInnerClass?: string //
}

export type InputGridColumnLabel =
  | {
      id: string
      label: string
      empty?: false
    }
  | { empty: true }
