import {
  createRouter,
  createWebHistory,
  RouterScrollBehavior,
} from 'vue-router'
import Home from '@/pages/home.md'
import Layout from '@/pages/layout.md'
import Buttons from '@/pages/buttons.md'
import Form from '@/pages/form.md'
import FormExample from '@/pages/form-example.md'
import InputGridExample from '@/pages/input-grid-example.md'
import Feedback from '@/pages/feedback.md'
import Content from '@/pages/content.md'

export const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      order: 1,
      iconLeft: 'home',
    },
  },
  {
    path: '/layout/',
    name: 'Layout',
    component: Layout,
    meta: {
      order: 2,
      iconLeft: 'rectangle-group',
    },
  },
  {
    path: '/buttons/',
    name: 'Buttons',
    component: Buttons,
    meta: {
      order: 3,
      iconLeft: 'cursor-arrow-rays',
    },
  },
  {
    path: '/form/',
    name: 'Form',
    component: Form,
    meta: {
      order: 4,
      iconLeft: 'pencil-square',
    },
  },
  {
    path: '/feedback/',
    name: 'Feedback',
    component: Feedback,
    meta: {
      order: 4,
      iconLeft: 'chat-bubble-left-ellipsis',
    },
  },
  {
    path: '/content/',
    name: 'Content',
    component: Content,
    meta: {
      order: 5,
      iconLeft: 'document-text',
    },
  },
  {
    path: '/form-example/',
    name: 'Example Contact Form',
    component: FormExample,
    meta: {
      hidden: true,
    },
  },
  {
    path: '/input-grid-example/',
    name: 'Input Grid Example',
    component: InputGridExample,
    meta: {
      hidden: true,
    },
  },
]

const scrollBehavior: RouterScrollBehavior = (_to, _from, savedPosition) => {
  if (savedPosition) {
    return savedPosition
  } else {
    return { top: 0 }
  }
}

export const routerOptions = {
  scrollBehavior,
}

const router = createRouter({
  history: createWebHistory(),
  routes,
  ...routerOptions,
})

export default router
