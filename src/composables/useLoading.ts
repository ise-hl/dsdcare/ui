import { shallowReadonly, ref } from 'vue'
import { promiseTimeout } from '@vueuse/core'
import { nanoid } from 'nanoid'
import { LoadingStepStatus } from '../enums'
import { LoadingStep } from '../propInterfaces'

/**
 * Additional options for {@link $loading.addStep}.
 */
export interface IAddStepOptions {
  /** Insert the step at `steps.value[index]` */
  index?: number
  /** Insert the step right after an existing step with `id === insertAfterId` */
  insertAfterId?: number | string
  /** Insert the step right before an existing step with `id === insertAfterId` */
  insertBeforeId?: number | string
}

/**
 * Additional options for {@link $loading.updateStep}.
 */
export interface IUpdateStepOptions {
  /** Replace the current step instead of merging with existing data */
  replace?: boolean
}

/**
 * Options for {@link $loading.asyncFn}.
 */
export interface IAsyncFnOptions<T> {
  /** The ID of an existing step */
  id?: number | string
  /** A new step to be added instead of using an existing step from the `steps` array */
  step?: LoadingStep | string
  /** A Promise or function to execute */
  fn: Promise<T> | (() => Promise<T>)
  /** Minimum duration of the routine in milliseconds (for UX purposes) */
  minDuration?: number
  /** Status during `fn` execution */
  statusRunning?: LoadingStepStatus
  /** Status to apply after completion of `minDuration` and `fn` execution */
  statusAfter?: LoadingStepStatus
  /** Status to apply if error is thrown during `fn` execution */
  statusError?: LoadingStepStatus
}

/** Whether to show (`isActive === true`) or to hide (`isActive === false`) a `UiLoading` component. */
const isActive = ref(false)

/** Steps to be displayed in a `UiLoading` component. */
const steps = ref<Array<LoadingStep>>([])

/**
 * Sets {@link isActive} to `true`.
 */
function show() {
  isActive.value = true
}

/**
 * Sets {@link isActive} to `false`.
 */
function hide() {
  isActive.value = false
}

/**
 * Adds a new step to the `steps` array.
 *
 * By default the step will be appended.
 *
 * If `step` is a string, it will be converted into an object with `label = step` and an `id`
 * generated via {@link nanoid}.
 *
 * @param step The step data
 * @param {IAddStepOptions} [options] Additional options.
 * @returns The inserted step.
 */
function addStep(
  step: LoadingStep | string,
  { index, insertAfterId, insertBeforeId }: IAddStepOptions = {},
): LoadingStep {
  let insertIndex: undefined | number = undefined

  if (index !== undefined) {
    insertIndex = index
  } else if (insertAfterId !== undefined) {
    insertIndex = steps.value.findIndex((step) => step.id === insertAfterId)
  } else if (insertBeforeId !== undefined) {
    insertIndex = steps.value.findIndex((step) => step.id === insertBeforeId)
  }

  if (insertIndex === -1) {
    insertIndex = undefined // reset since `insertAfterId` or `insertBeforeId` did not match a step
  }

  // convert to object and generate `id` if necessary
  const newStep =
    typeof step === 'string' ? { id: nanoid(), label: step } : step

  steps.value.splice(insertIndex ?? steps.value.length, 0, newStep)

  return newStep
}

/**
 * Updates the step identified by `id`.
 *
 * By default, the new data provided via `step` will be merged into the existing data of the
 * step. This behavior can be turned off via `options.replace`!
 */
function updateStep(
  id: number | string,
  step: LoadingStep | Partial<LoadingStep>,
  options?: { replace: false },
): void
function updateStep(
  id: number | string,
  step: LoadingStep | string,
  options: { replace: true },
): LoadingStep
function updateStep(
  id: number | string,
  step: LoadingStep | Partial<LoadingStep> | string,
  options?: IUpdateStepOptions,
) {
  const index = steps.value.findIndex((step) => step.id === id)

  if (index === -1) {
    throw new Error('Step not found')
  }

  if (options?.replace) {
    steps.value[index] = (
      typeof step === 'string' ? { label: step, id: nanoid() } : step
    ) as LoadingStep

    return steps.value[index] // return required since `id` changes
  }

  // merge current step data with new data
  steps.value[index] = {
    ...steps.value[index],
    ...(step as LoadingStep),
  }
}

/**
 * Removes the step identified by `id`.
 */
function removeStep(id: number | string) {
  const index = steps.value.findIndex((step) => step.id === id)

  if (index === -1) {
    throw new Error('Step not found')
  }

  steps.value.splice(index, 1)
}

/**
 * Removes all steps from the `steps` array.
 */
function clearSteps() {
  steps.value = []
}

/**
 * Execute an async function or Promise and manage status updates of a `LoadingStep`.
 *
 * The status of the specified `step` (alternatively specified through `id`) will be updated
 * according to the execution status of `fn`.
 *
 * For UX purposes, a minimum exection duration can be specified, e.g. to ensure that a LoadingStep
 * will be visible if `fn` is executed very quickly.
 *
 * @returns The awaited result of `fn`
 */
async function asyncFn<T>({
  id,
  step,
  fn,
  minDuration,
  statusRunning = LoadingStepStatus.Running,
  statusAfter = LoadingStepStatus.Done,
  statusError = LoadingStepStatus.Error,
}: IAsyncFnOptions<T>) {
  if (step) {
    /* add new step */
    id = $loading.addStep(step).id
  } else if (id === undefined || !steps.value.find((step) => step.id === id)) {
    throw Error('No valid existing or new step specified!')
  }

  updateStep(id, { status: statusRunning })

  try {
    /* execute `fn` along with promiseTimeout which applies a min duration */
    const [fnResult] = await Promise.all([
      typeof fn === 'function' ? fn() : fn,
      ...(minDuration ? [promiseTimeout(minDuration)] : []),
    ])

    updateStep(id, { status: statusAfter })

    return fnResult
  } catch (error) {
    updateStep(id, { status: statusError })
    throw error
  }
}

const $loading = shallowReadonly({
  isActive: isActive,
  steps: steps,
  show,
  hide,
  addStep,
  updateStep,
  removeStep,
  clearSteps,
  asyncFn,
})

/**
 * A singleton that serves as a service to use an instance of `UiLoading` on the app root from
 * child components.
 *
 * The methods provided by `$loading` can be used to control `isActive` and `steps`.
 * Alternatively, both properties can be mutated directly if preferred.
 */
export const useLoading = () => {
  return { isActive, steps, $loading }
}
