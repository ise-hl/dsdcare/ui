---
sidebarNavItems:
  [
    { href: '#default-variants', label: 'Default variants' },
    { href: '#light-variants', label: 'Light variants' },
    { href: '#link-buttons', label: 'Link buttons' },
    { href: '#disabled-state', label: 'Disabled state' },
    { href: '#icon-buttons', label: 'Icon buttons' },
    { href: '#icon-only-buttons', label: 'Icon-only buttons' },
    { href: '#loading-state', label: 'Loading state' },
    { href: '#disabled-loading-state', label: 'Disabled loading state' },
  ]
---

# Buttons

_**Note:** The `button-grid` class used in the following code snippets is only used for layout purposes on this demo page. It is defined as follows:_

```css
.button-grid {
  @apply flex;
  @apply flex-wrap;
  @apply gap-4;
}
```

## Default variants

<examples-buttons-default />
<docs-code-snippet>

```html
<div class="button-grid">
  <ui-button>Default</ui-button>
  <ui-button info>Info</ui-button>
  <ui-button success>Success</ui-button>
  <ui-button warning>Warning</ui-button>
  <ui-button danger>Danger</ui-button>
  <ui-button gray>Gray</ui-button>
</div>
```

</docs-code-snippet>

## Light variants

<examples-buttons-light />
<docs-code-snippet>

```html
<div class="button-grid">
  <ui-button light>Default Light</ui-button>
  <ui-button info light>Info Light</ui-button>
  <ui-button success light>Success Light</ui-button>
  <ui-button warning light>Warning Light</ui-button>
  <ui-button danger light>Danger Light</ui-button>
  <ui-button gray light>Gray</ui-button>
</div>
```

</docs-code-snippet>

## Link buttons

<examples-buttons-link />
<docs-code-snippet>

```html
<div class="button-grid">
  <ui-button tag="a" href="#link-buttons">Default Link</ui-button>
  <ui-button tag="a" href="#link-buttons" info>Info Link</ui-button>
  <ui-button tag="a" href="#link-buttons" success>Success Link</ui-button>
  <ui-button tag="a" href="#link-buttons" warning>Warning Link</ui-button>
  <ui-button tag="a" href="#link-buttons" danger>Danger Link</ui-button>
  <ui-button tag="a" href="#link-buttons" gray>Gray Link</ui-button>
</div>
```

</docs-code-snippet>

## Disabled state

<examples-buttons-disabled />
<docs-code-snippet>

```html
<div class="button-grid">
  <ui-button disabled>Default</ui-button>
  <ui-button disabled info>Info</ui-button>
  <ui-button disabled success>Success</ui-button>
  <ui-button disabled warning>Warning</ui-button>
  <ui-button disabled danger>Danger</ui-button>
  <ui-button disabled gray>Gray</ui-button>
</div>
```

</docs-code-snippet>

## Icon buttons

<examples-buttons-icon />
<docs-code-snippet>

```html
<template>
  <div class="button-grid">
    <ui-button>
      <template #iconLeft>
        <arrow-left-icon class="stroke-2" />
      </template>
      Icon left
    </ui-button>
    <ui-button>
      Icon right
      <template #iconRight>
        <arrow-right-icon class="stroke-2" />
      </template>
    </ui-button>
  </div>
</template>

<script lang="ts" setup>
  import { ArrowLeftIcon, ArrowRightIcon } from '@heroicons/vue/24/outline'
</script>
```

</docs-code-snippet>

## Icon-only buttons

<examples-buttons-icon-only />
<docs-code-snippet>

```html
<template>
  <div class="button-grid">
    <ui-button icon-only>
      <template #iconLeft>
        <arrow-left-icon class="stroke-2" />
      </template>
      Icon left
    </ui-button>
    <ui-button icon-only>
      Icon right
      <template #iconRight>
        <arrow-right-icon class="stroke-2" />
      </template>
    </ui-button>
  </div>
</template>

<script lang="ts" setup>
  import { ArrowLeftIcon, ArrowRightIcon } from '@heroicons/vue/24/outline'
</script>
```

</docs-code-snippet>

## Loading state

<examples-buttons-loading />
<docs-code-snippet>

```html
<div class="button-grid">
  <ui-button loading>Default</ui-button>
  <ui-button loading info>Info</ui-button>
  <ui-button loading success>Success</ui-button>
  <ui-button loading warning>Warning</ui-button>
  <ui-button loading danger>Danger</ui-button>
  <ui-button loading gray>Gray</ui-button>
</div>
```

</docs-code-snippet>

<examples-buttons-loading-light />
<docs-code-snippet>

```html
<div class="button-grid">
  <ui-button loading light>Default</ui-button>
  <ui-button loading info light>Info</ui-button>
  <ui-button loading success light>Success</ui-button>
  <ui-button loading warning light>Warning</ui-button>
  <ui-button loading danger light>Danger</ui-button>
  <ui-button loading gray light>Gray</ui-button>
</div>
```

</docs-code-snippet>

## Disabled loading state

<examples-buttons-loading-disabled />
<docs-code-snippet>

```html
<div class="button-grid">
  <ui-button disabled loading>Default</ui-button>
  <ui-button disabled loading info>Info</ui-button>
  <ui-button disabled loading success>Success</ui-button>
  <ui-button disabled loading warning>Warning</ui-button>
  <ui-button disabled loading danger>Danger</ui-button>
  <ui-button disabled loading gray>Gray</ui-button>
</div>
```

</docs-code-snippet>

<examples-buttons-loading-disabled-light />
<docs-code-snippet>

```html
<div class="button-grid">
  <ui-button disabled loading light>Default</ui-button>
  <ui-button disabled loading info light>Info</ui-button>
  <ui-button disabled loading success light>Success</ui-button>
  <ui-button disabled loading warning light>Warning</ui-button>
  <ui-button disabled loading danger light>Danger</ui-button>
  <ui-button disabled loading gray light>Gray</ui-button>
</div>
```

</docs-code-snippet>

<style lang="postcss">
  .button-grid {
    @apply flex;
    @apply flex-wrap;
    @apply gap-4;
  }
</style>
