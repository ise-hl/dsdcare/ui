import { createApp } from 'vue'
import App from '@/App.vue'
import Router from '@/router/index'

import 'virtual:svg-icons-register'

import 'inter-ui/inter-variable.css'
import 'prism-themes/themes/prism-one-dark.min.css'
import '@/assets/index.css'

const app = createApp(App)
app.use(Router)
app.mount('#app')
