# Input grid example

The following example demonstrates the usage of the `UiInputGrid` component with `UiRadioGroup` and `UiCheckboxGroup` items.

<ui-call-out>

**Note:** Currently, other input components are not supported by the `UiInputGrid`, i.e. they have not been adapted, yet. You may instead apply the CSS class `col-span-full` (along with `w-full`, if preferred) to the unsupported components.

</ui-call-out>

<examples-form-input-grid />

<docs-code-snippet>

```html
<template>
  <div class="my-8">
    <ui-checkbox-group
      :options="toggleOptions"
      label="UiInputGrid config"
      hints="Use the following toggles to test the corresponding props."
    >
      <template #default="{ options }">
        <div class="flex gap-4 p-0">
          <ui-checkbox
            v-for="option of options as string[]"
            :key="option"
            v-model="toggle"
            :name="option"
            :value="option"
            :label="option"
            button
          />
        </div>
      </template>
    </ui-checkbox-group>
  </div>

  <ui-input-grid
    :label="recordSchema.label"
    hints="Bitte beantworten Sie jede Teilfrage!"
    :columns="gridCols + 1"
    :column-labels="
      itemOptions.map((option) => ({
        id: option.labelledById,
        label: option.label,
      }))
    "
    :sticky="toggle.includes('sticky') ? `${navbarHeight ?? 0}px` : ''"
  >
    <template #preColumnLabels="{ tooNarrow }">
      <ui-call-out v-if="tooNarrow" warning icon="⚠️">
        Too narrow, grid disabled
      </ui-call-out>
    </template>

    <template #default="{ tooNarrow }">
      <component
        :is="components[item.component]"
        v-for="item of getItems(!tooNarrow)"
        :key="item.model"
        v-model="model[item.model]"
        v-bind="item"
        :errors="toggle.includes('errors') && getErrorMessage(item.model)"
        :highlighted="toggle.includes('highlighted') && !isValid(item.model)"
        :is-resettable="!item.required"
        :is-grid-child="!tooNarrow"
      ></component>
    </template>
  </ui-input-grid>
</template>

<script lang="ts" setup>
  import type { Component } from 'vue'
  import { computed, inject, ref, Ref } from 'vue'
  import {
    UiCheckbox,
    UiCheckboxGroup,
    UiInputGrid,
    UiRadioGroup,
  } from '@ise-hl/dsdcare-ui'
  import recordSchema from './InputGridSchema'

  const components: Record<string, string | Component> = {
    checkbox: UiCheckboxGroup,
    radio: UiRadioGroup,
  }

  const navbarHeight = inject<Ref<number>>('navbarHeight')

  const model = ref<Record<string, string | number | Array<string | number>>>(
    {}
  )
  const toggleOptions = ['sticky', 'highlighted', 'errors']
  const toggle = ref(['sticky'])

  const itemOptions = computed(() =>
    recordSchema.schema[0].options.map((option) => ({
      ...option,
      labelledById: `${recordSchema.model}:option-${option.id}`,
    })),
  )

  const getItems = (externalLabels: boolean) => {
    if (externalLabels) {
      return recordSchema.schema.map((item) => ({
        ...item,
        options: itemOptions.value,
      }))
    }

    return recordSchema.schema
  }

  const gridCols = computed(() =>
    itemOptions.value ? itemOptions.value.length + 1 : 0,
  )

  const isValid = (id: string) => {
    const value = model.value[id]

    return !(!value || (Array.isArray(value) && !value.length))
  }

  const getErrorMessage = (id: string) => {
    return isValid(id) ? '' : 'Bitte beantworten Sie diese Teilfrage!'
  }
</script>
```

</docs-code-snippet>

## Using sticky column labels

Since the component does not know anything abouts its surrounding context, you must provide the starting position manually via the `sticky` prop if you want to use sticky column labels. Internally, the value passed to `sticky` is used as the `top` CSS property. Consequently, the value must be provided as a string and include the corresponding CSS unit. The previous example uses the height of the navbar which is provided by the App as follows:

```html
<template>
  <ui-navbar ref="navbar" sticky />
</template>

<script lang="ts" setup>
  import { computed, provide, ref } from 'vue'
  import { useElementSize } from '@vueuse/core'

  const navbar = ref(null)
  const { height: navbarHeight } = useElementSize(navbar)

  provide(
    'navbarHeight',
    computed(() => Math.floor(navbarHeight.value))
  )
</script>
```
