<template>
  <div
    ref="parent"
    class="w-full"
    :style="{
      '--columnGap': `${columnGap * 0.25}rem`,
      '--firstColFr': `${columns}fr`,
      '--remainingCols': columns - 1,
    }"
  >
    <fieldset
      ref="grid"
      class="ui-input-grid"
      :class="{
        'grid grid-cols-[minmax(min-content,var(--firstColFr))_repeat(var(--remainingCols),minmax(min-content,1fr))] items-center justify-items-center gap-x-[var(--columnGap)]':
          !tooNarrow,
        'too-narrow space-y-8': tooNarrow,
        'sticky-column-labels': sticky,
      }"
      :aria-describedby="hintsId"
    >
      <legend
        v-if="label"
        class="col-span-full"
        :class="
          customLabelClass
            ? customLabelClass
            : 'mb-2 text-lg font-bold md:text-xl lg:text-2xl'
        "
      >
        {{ label }}
      </legend>

      <ui-form-hints
        v-if="!tooNarrow"
        :hints="hints"
        :hints-id="hintsId"
        class="col-span-full mb-4 w-full"
      />

      <slot name="preColumnLabels" :too-narrow="tooNarrow" />

      <div v-if="!tooNarrow && columnLabels?.length" class="contents">
        <div
          v-if="firstLabelledColumn > 1"
          class="-mx-[var(--columnGap)] flex items-end justify-center place-self-stretch self-stretch border-b-2 border-b-gray-700 p-[var(--columnGap)] [border-image:linear-gradient(to_right,transparent_0_theme(padding.2),theme(colors.gray.700)_theme(padding.2)_100%)_1]"
          :class="{
            'bg-white': !sticky,
            'sticky z-10 bg-transparent bg-gradient-to-r from-white/90 via-white/90 to-white':
              sticky,
          }"
          role="presentation"
          :style="{
            'grid-column': `span ${firstLabelledColumn - 1}`,
            top: sticky,
          }"
        ></div>

        <template v-for="(column, index) of columnLabels" :key="column.id">
          <div
            class="-mx-[var(--columnGap)] flex items-end justify-center place-self-stretch self-stretch border-b-2 border-b-gray-700 bg-white p-[var(--columnGap)]"
            :class="{
              '[border-image:linear-gradient(to_right,transparent_0_theme(padding.2),theme(colors.gray.700)_theme(padding.2)_100%)_1]':
                index === 0 && firstLabelledColumn === 1,
              '-mr-1 pr-1': firstLabelledColumn + index === columns,
              'sticky z-10': sticky,
            }"
            :style="{
              top: sticky,
            }"
          >
            <span
              v-if="!column.empty"
              :id="column.id"
              class="font-medium leading-tight"
              >{{ column.label }}</span
            >
            <span v-else :key="index" aria-hidden></span>
          </div>
        </template>

        <div
          v-if="columnSpanAfterColumnLabels > 0"
          class="-mx-[var(--columnGap)] -mr-1 flex items-end justify-center place-self-stretch self-stretch border-b-2 border-b-gray-700 bg-white p-[var(--columnGap)] pr-1"
          :class="{ 'sticky z-10': sticky }"
          role="presentation"
          :style="{
            'grid-column': `span ${columnSpanAfterColumnLabels}`,
            top: sticky,
          }"
        ></div>
      </div>
      <slot :too-narrow="tooNarrow" />
    </fieldset>
  </div>
</template>

<script lang="ts" setup>
import type { PropType } from 'vue'
import type { InputGridColumnLabel } from '@/propInterfaces'
import { computed, ref, watch } from 'vue'
import { useDebounceFn, useElementSize } from '@vueuse/core'
import { nanoid } from 'nanoid'
import UiFormHints from './FormHints.vue'

defineOptions({
  name: 'UiInputGrid',
})

const props = defineProps({
  label: {
    type: String,
    default: undefined,
  },
  customLabelClass: {
    type: String,
    default: undefined,
  },
  hints: {
    type: [Array, String] as PropType<string[] | string>,
    default: () => [],
    validate: (hints: [] | string) =>
      Array.isArray(hints)
        ? hints.every((item) => typeof item === 'string')
        : typeof hints === 'string',
  },
  columns: {
    type: Number,
    default: 1,
  },
  columnLabels: {
    type: Array as PropType<InputGridColumnLabel[]>,
    default: () => [],
  },
  /**
   * One-based index of the first column that is used as a column label.
   */
  firstLabelledColumn: {
    type: Number,
    default: 2,
  },
  columnGap: {
    type: Number,
    default: 3,
  },
  sticky: {
    type: String,
    required: false,
    default: undefined,
  },
})

const hintsId = nanoid()

const tooNarrow = ref(false)
const parent = ref<HTMLFieldSetElement | null>(null)
const grid = ref<HTMLFieldSetElement | null>(null)
const { width: parentWidth } = useElementSize(parent)
const { width: gridWidth } = useElementSize(grid)

const updateTooNarrow = useDebounceFn(
  (
    parentWidth?: number,
    gridWidth?: number,
    prevParentWidth?: number,
    prevGridWidth?: number,
  ) => {
    if (!parentWidth || !gridWidth) {
      return
    }

    const previous = {
      tooNarrow: tooNarrow.value,
      parentWidth: prevParentWidth,
      gridWidth: prevGridWidth,
    }

    if (gridWidth > parentWidth && previous.tooNarrow === false) {
      /* The grid is wider than the parent; previously it was not too wide. */
      tooNarrow.value = true
    } else if (
      gridWidth <= parentWidth &&
      (!previous.parentWidth || previous.parentWidth < parentWidth)
    ) {
      /**
       * `tooNarrow` is only set to `true` if the previous state was set with a lower `parentWidth`.
       * This prevents an endless loop potentially caused by `gridWidth` being smaller than
       * `parentWidth` once `tooNarrow` has disabled the grid layout.
       */

      tooNarrow.value = false
    }
  },
  50,
)

watch(
  [parentWidth, gridWidth],
  ([parentWidth, gridWidth], [prevParentWidth, prevGridWidth]) => {
    updateTooNarrow(parentWidth, gridWidth, prevParentWidth, prevGridWidth)
  },
  { immediate: true },
)

const columnSpanAfterColumnLabels = computed(
  () =>
    props.columns -
    (props.columnLabels?.length ?? 0) -
    (props.firstLabelledColumn - 1),
)
</script>
