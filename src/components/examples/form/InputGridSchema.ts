export const InputGridGroupRecordSchema = {
  model: 'urn:osse-16:record:8:1',
  label:
    'Bitte geben Sie an, ob und in welchem Umfang Sie die folgenden Leistungen erhalten haben, ob Ihr Kind diese Leistungen Ihrer Meinung nach braucht oder ob es sie nicht benötigt.',
  schema: [
    {
      model: 'urn:osse-16:dataelement:248:1',
      label: 'Psychologische Beratung / Psychotherapie',
      component: 'radio',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie eine Antwort aus.',
    },
    {
      model: 'urn:osse-16:dataelement:249:1',
      label: 'Beratung durch Sozialdienst',
      component: 'radio',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie eine Antwort aus.',
    },
    {
      model: 'urn:osse-16:dataelement:250:1',
      label: 'Fertilitätsberatung',
      component: 'radio',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie eine Antwort aus.',
    },
    {
      model: 'urn:osse-16:dataelement:251:1',
      label: 'Schulung (über Varianten der Geschlechtsentwicklung)',
      component: 'radio',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie eine Antwort aus.',
    },
    {
      model: 'urn:osse-16:dataelement:252:1',
      label: 'Hilfe bei der Koordination verschiedener Gesundheitsdienste',
      component: 'radio',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie eine Antwort aus.',
    },
    {
      model: 'urn:osse-16:dataelement:253:1',
      label: 'Selbsthilfegruppen',
      component: 'radio',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie eine Antwort aus.',
    },
    {
      model: 'urn:osse-16:dataelement:254:1',
      label: 'Sexualberatung',
      component: 'radio',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie eine Antwort aus.',
    },
    {
      model: 'urn:osse-16:dataelement:255:1',
      label: 'Ausführliche Telefonberatung durch medizinisches Fachpersonal',
      component: 'checkbox',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie alle zutreffenden Antworten aus.',
    },
    {
      model: 'urn:osse-16:dataelement:256:1',
      label:
        'Gesundheitsdienstleistungen in der Schule, der Ausbildung oder im Beruf',
      component: 'checkbox',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie alle zutreffenden Antworten aus.',
    },
    {
      model: 'urn:osse-16:dataelement:257:1',
      label: 'Ernährungsberatung / -therapie',
      component: 'checkbox',
      required: false,
      options: [
        {
          id: '1',
          label: 'ja',
        },
        {
          id: '2',
          label: 'ja, teilweise',
        },
        {
          id: '3',
          label: 'nein, brauchen wir nicht',
        },
        {
          id: '4',
          label: 'nein, würden wir aber benötigen',
        },
      ],
      hints: 'Bitte wählen Sie alle zutreffenden Antworten aus.',
    },
  ],
}

export default InputGridGroupRecordSchema
