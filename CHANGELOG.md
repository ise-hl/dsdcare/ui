# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.2] - 2024-01-19

### Changed

- Updated usage instructions in docs.

## [1.4.1] - 2024-01-18

### Changed

- Updated `autoprefixer`, `prettier`, and `vue` to their latest versions.
- Improved hover and focus colors of `UiNavbarListItem` and `UiLanguageSelect` (#23).

### Fixed

- The initial selection of an answer and the reset action of `UiRadioGroup` with `isResettable` set to `true` caused layout shifts (#22).

## [1.4.0] - 2024-01-16

**BREAKING:** The stylesheet `@ise-hl/dsdcare-ui/dist/style.css` is not bundled anymore since all styles are now included in the respective components. Please remove the import in your app's stylesheet.

### Added

- Support [`inter-ui`](https://github.com/philipbelesky/inter-ui) v4 in the [Tailwindcss preset](tailwind.preset.cjs).

### Changed

- Updated `@headlessui/vue`, `@heroicons/vue`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `eslint`, `eslint-plugin-prettier`, `eslint-plugin-vue`, `inter-ui`, `postcss`, `postcss-html`, `postcss-import`, `postcss-nesting`, `prettier`, `prettier-plugin-tailwindcss`, `tailwindcss`, `vite`, `vite-plugin-dts`, `vue`, `vue-eslint-parser`, and `vue-tsc` to their latest versions.
- Converted all custom CSS classes to inline Tailwindcss classes (#16). As a result, the previously bundled stylesheet `@ise-hl/dsdcare-ui/dist/style.css` is not generated and accordingly can't be imported anymore. Compared to the previous implementation, this makes it possible to tree shake styles from unused components.

### Fixed

- The reset button of `UiRadioGroup` was displayed and usable even if `disabled` was set to `true` (#18).
- The `cursor: pointer` style was incorrectly applied to parts of input component that are not interactive (`UiFormErrors` and `UiFormHints`) (#19).
- The computed property `selected` of the `UiLanguageSelect` component did not work as intended (#21). The getter did not return the current value and the setter could not update its value.

## [1.3.0] - 2023-12-11

### Changed

- Updated `@tailwindcss/forms`, `@tailwindcss/typography`, `nanoid`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vue/eslint-config-typescript`, `@vueuse/core`, `autoprefixer`, `eslint`, `eslint-config-prettier`, `eslint-plugin-prettier`, `eslint-plugin-vue`, `lint-staged`, `postcss`, `prettier`, `prettier-plugin-tailwindcss`, `tailwindcss`, `typescript`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, `vue`, `vue-eslint-parser`, `vue-router`, and `vue-tsc` to their latest versions.
- Updated `stylelint` to its latest minor version.
- Replaced `vite-plugin-md` with `unplugin-vue-markdown`.
- Updated icons used from [heroicons](https://heroicons.com/) to variants of the current v2.
- First public release licensed under [BSD-3-Clause](https://opensource.org/license/bsd-3-clause/). 🎉

### Fixed

- Docs: Global components are now correctly resolved in markdown files in production, eliminating the need for the previously required workaround.

## [1.2.2] - 2023-08-24

**BREAKING:** As of this release we use [pnpm](https://pnpm.io/) instead of npm to manage dependencies.

### Changed

- Updated `@headlessui/vue`, `@tailwindcss/forms`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `autoprefixer`, `eslint`, `eslint-config-prettier`, `eslint-plugin-prettier`, `eslint-plugin-vue`, `lint-staged`, `postcss`, `prettier`, `prettier-plugin-tailwindcss`, `stylelint`, and `vite-plugin-dts` to their latest versions.
- Replaced `stylelint-config-standard` with `stylelint-config-recommended-vue`.

## [1.2.1] - 2023-07-07

### Changed

- Updated `@headlessui/vue`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vueuse/core`, `eslint`, `eslint-plugin-vue`, `lint-staged`, `postcss`, `postcss-nesting`, `stylelint`, `stylelint-config-standard`, `typescript`, `unplugin-vue-components`, `vite`, `vue-eslint-parser`, `vue-router`, and `vue-tsc` to their latest versions.

### Fixed

- Incompatibility with `tsconfig.json` option `resolvePackageJsonImports` due to incorrect `exports` definition in `package.json`.

## [1.2.0] - 2023-05-31

**BREAKING:** This release requires `@vueuse/core` >= 10.1.0, `vue` >= 3.3.0, and `vue-router` >= 4.2.0! Additionally, the minimum version of Node is now v18!

### Added

- Types: `InputGridColumnLabel` for the `columnLabel` prop of the `UiInputGrid` component (#13).

### Changed

- Updated `@headlessui/vue`, `nanoid`, `@commitlint/cli`, `@commitlint/config-conventional`, `@vitejs/plugin-vue`, `@vue/eslint-config-typescript`, `@vueuse/core`, `autoprefixer`, `eslint`, `eslint-config-prettier`, `eslint-plugin-vue`, `lint-staged`, `markdown-it-anchor`, `postcss`, `postcss-nesting`, `prettier`, `stylelint`, `stylelint-config-standard`, `tailwindcss`, `typescript`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, `vue`, `vue-router`, and `vue-tsc` to their latest versions.
- Removed dev dependency `stylelint-config-prettier` that is no longer needed.
- Removed WIP notice on home page.

## [1.1.1] - 2023-02-16

### Fixed

- Docs: `UiCallOut` and `DocsCodeSnippet` were not imported on the page `input-grid-example`.

## [1.1.0] - 2023-02-16

### Added

- `UiInputGrid` component, which serves as a wrapper for input components while applying a grid based layout (#12).

### Changed

- `UiCheckbox` and `UiRadio` now support external labels via the `labelledById` prop (#12). This allows usage within a grid layout in a matrix question style.
- `UiCheckboxGroup` and `UiRadioGroup` can be used within grid layouts (#12). The label and answer options are spread into the available grid columns.
- Updated `@headlessui/vue`, `nanoid`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `eslint`, `lint-staged`, `postcss-nesting`, `prettier`, `tailwindcss`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, and `vue` to their latest versions.

## [1.0.4] - 2023-02-01

### Changed

- Updated `@headlessui/vue`, `@tailwindcss/typography`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vueuse/core`, `eslint`, `eslint-plugin-vue`, `postcss-nesting`, `prettier`, `typescript`, `unplugin-vue-components`, and `vite-plugin-dts` to their latest versions.

### Fixed

- The legend of the `UiRadioGroup` component was not picked up by screenreaders (#10).
- Broken tab navigation for `UiRadio` and `UiRadioGroup` (#11).

## [1.0.3] - 2023-01-12

### Fixed

- The module did not export `tailwind.preset`.

## [1.0.2] - 2023-01-10

### Changed

- The package is now internally set up as an ES module.
- Updated `@headlessui/vue`, `@tailwindcss/typography`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `autoprefixer`, `eslint`, `eslint-config-prettier`, `eslint-plugin-vue`, `husky`, `lint-staged`, `markdown-it-anchor`, `postcss`, `prettier`, `stylelint`, `stylelint-config-prettier`, `tailwindcss`, `typescript` `unplugin-vue-components`, `vite`, `vite-plugin-dts`, `vite-plugin-md`, `vue`, and `vue-tsc` to their latest versions.

## [1.0.1] - 2022-10-25

### Changed

- Use `defineComponent` on all components to improve type declarations (#9).
- Updated `@headlessui/vue`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `eslint`, `eslint-plugin-vue`, `postcss`, `stylelint`, `stylelint-config-standard`, `tailwindcss`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, `vue`, `vue-router`, and `vue-tsc` to their latest versions.

## [1.0.0] - 2022-09-29

**BREAKING:** This release marks the completion of the transition to Vue 3.2, older versions of Vue are not supported anymore!

### Changed

- Updated `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vueuse/core`, `eslint`, `stylelint`, `typescript`, `vite`, `vite-plugin-dts`, and `vue` to their latest versions.

## [0.32.1-vite] - 2022-09-23

### Changed

- Updated `@vue/eslint-config-typescript`, `autoprefixer`, and `stylelint` to their latest versions.

### Fixed

- Broken examples and code snippets in docs.
- Reset button of `UiRadioGroup` overlapping hints text on small viewports.

## [0.32.0-vite] - 2022-09-20

### Added

- `isResettable` variant of `UiRadioGroup` which allows undoing the selection via a reset button (#8).

### Changed

- Updated `@headlessui/vue`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `autoprefixer`, `eslint-plugin-vue`, `markdown-it-anchor`, `postcss-nesting`, `stylelint`, and `vite` to their latest versions.

## [0.31.2-vite] - 2022-09-12

### Fixed

- `onClick` handlers were not passed to `NavbarListItem` instances.

## [0.31.1-vite] - 2022-09-08

### Fixed

- Props were passed to the `NavbarListItem` even if they were `undefined` which caused `RouterLinks` to generate invalid `a` elements.

## [0.31.0-vite] - 2022-09-08

### Added

- Alternative route-matching mode for `SidebarNavItem` which enables active styling for non-exact route matching, i.e. when child routes are active. Can be enabled by passing `matchNonExactRoute = true` to the component.
- `customClass` and `customInnerClass` props to `SidebarNavItem` to allow style customizations.

### Changed

- **BREAKING:** Rename the prop `hasOrderedItems` of `SidebarNavItem` to `parentIsOrderedList` to clarify its relation to the parent list.
- Explicitly bind properties of `Navbar` items to `NavbarListItem`s to prevent additional properties unused by the `NavbarListItem` from being present in the DOM.
- Loosen default route matching behavior of `NavbarListItem`s to apply active styling on non-exact route matches, i.e. when child routes are active. Can be disabled by passing `matchNonExactRoute = false` to the component.
- Improve inheritance of list type (ordered/unordered) in `SidebarNavItem`.
- Updated `vue` and `vue-tsc` to their latest versions.

## [0.30.0-vite] - 2022-09-07

### Added

- Components that internally use `RouterLink`s and have corresponding props with a `to` property are now typed to support `RouteLocationRaw` on the `to` property.
- Make `to` property of `BreadcrumbItem` optional to support text-only breadcrumb segments.

### Changed

- Updated `@headlessui/vue`, `@vue/eslint-config-typescript`, and `vue-tsc` to their latest versions.

### Fixed

- Active `RouterLink` styles were applied to instances of `SidebarNavItem` with non-exact route matches.

## [0.29.2-vite] - 2022-09-06

### Fixed

- Incorrect left margin on second level sidebar nav items if the first level (top level) is unordered.
- Sidebar nav item padding to prevent nav item text being covered by the hover or active router link background color on some screen sizes.

## [0.29.1-vite] - 2022-09-06

### Changed

- Updated `@tailwindcss/forms`, `@tailwindcss/typography`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `eslint`, `eslint-plugin-vue`, `markdown-it-prism`, `postcss`, `stylelint`, `stylelint-config-standard`, `tailwindcss`, `typescript`, `unplugin-vue-components`, `vite`, `vite-plugin-md`, `vue`, `vue-router`, and `vue-tsc` to their latest versions.

### Fixed

- Wrapped text of sidebar nav child items is partially covered by background decoration (#6).
- Top-level sidebar nav item markers of unordered lists not hidden (#7).

## [0.29.0-vite] - 2022-08-05

### Added

- `highlighted` modifier for input components (#5).

### Changed

- Updated `@vueuse/core` and `unplugin-vue-components` to their latest versions.

## [0.28.1-vite] - 2022-08-03

### Added

- Changelog for all previous releases.

### Changed

- Updated `@headlessui/vue`, `@tailwindcss/typography`, `nanoid`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `autoprefixer`, `eslint`, `eslint-plugin-prettier`, `eslint-plugin-vue`, `lint-staged`, `postcss-html`, `postcss-nesting`, `prettier`, `tailwindcss`, `typescript`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, `vite-plugin-md`, `vue-eslint-parser`, `vue-router`, and `vue-tsc` to their latest versions.

## [0.28.0-vite] - 2022-06-14

### Added

- Support overflowing content in `UiDialog` via vertical scrolling (be13d3157b979667af461fbd82f736b1f190a64c).

### Changed

- Updated `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vue/eslint-config-typescript`, `eslint-plugin-vue`, `prettier`, `stylelint`, `tailwindcss`, `vite`, `vite-plugin-md`, `vue-router`, and `vue-tsc` to their latest versions.

## [0.27.2-vite] - 2022-06-09

### Fixed

- `UiDialog` not included in library build (ae9395bdc5ffc427de5c74bafdaa2948ebd6583b).
- `@headlessui/vue` included in the build (a28b0ecb8c973d4c0c84f7771f9a7aecd3cb8e95).

## [0.27.1-vite] - 2022-06-09

### Fixed

- Build failing due to issues with peer dependencies.

## [0.27.0-vite] - 2022-06-09

### Added

- `UiDialog`: A dialog (modal) component based on [@headlessui/vue](https://headlessui.dev/vue/dialog) (9d710847397c75fc7d7e626f94d1b8ac1126c9dc).

### Changed

- Move various interface exports from SFCs to `propInterfaces.ts` to improve reusability and resolve the potential issue of default exports not being properly identified by the compiler (0adecbbd065f863ccff2aa170beaed1e35d44e0d).
- Updated `@tailwindcss/forms`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vueuse/core`, `eslint`, `eslint-plugin-vue`, `lint-staged`, `postcss`, `postcss-nesting`, `stylelint`, `stylelint-config-standard`, `tailwindcss`, `typescript`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, `vite-plugin-md`, `vue`, and `vue-tsc` to their latest versions.

### Fixed

- Text color inherited from parent component for some variants of `UiButton` (3c0335551b82fa3235af66dd7ca00207bcc3cc98).

## [0.26.3-vite] - 2022-05-18

### Changed

- Updated custom `UiRadioGroup` and `UiCheckboxGroup` examples (docs).
- Updated `@commitlint/cli`, `@commitlint/config-conventional`, `@vueuse/core`, and `eslint-plugin-vue` to their latest versions.

### Fixed

- Broken reactivity of `UiSidebarNavItem` props (2a536c0fa480710805cdc4c369eb9c7adabda0ac).

## [0.26.2-vite] - 2022-05-12

### Changed

- **BREAKING:** Dropped support for Node.js v12.
- Updated VSCode extension recommendations and workspace settings.
- Updated `@tailwindcss/forms`, `nanoid`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `autoprefixer`, `eslint`, `husky`, `markdown-it-anchor`, `postcss-nesting`, `stylelint`, `unplugin-vue-components`, `vite`, `vite-plugin-md`, `vue-eslint-parser`, `vue-router`, and `vue-tsc` to their latest versions.

## [0.26.1-vite] - 2022-05-02

### Fixed

- `fn` not handled properly in `asyncFn` of the `useLoading` composable (19ab736395554ed77cc13d8570c4f329916fffa0).

## [0.26.0-vite] - 2022-05-02

### Added

- `id` property to the `LoadingStep` interface (46e0292c4a52d46aeacc3ae08c7752e3837fef2d).
- `asyncFn` (`useLoading` composable): Executes an async function or awaits a promise and manages the status updates of a `LoadingStep` (0104b0aad3ee4ca06fc275c80a6456c94334771a).

### Changed

- Loading steps are now tracked by their ID in the `useLoading` composable (77c14cfc2f96e10529391b2083501c900ff9ebc7).
- Improved status validation in `UiLoadingStep` (f6d0e8c8743cc6b80b231652d558cbebecd697d5).
- Updated `@commitlint/cli`, `@commitlint/config-conventional`, `postcss`, `stylelint`, `typescript`, and `vue-tsc` to their latest versions.

## [0.25.5-vite] - 2022-04-28

### Changed

- Improved credential handling for `docker login` (CI).

## [0.25.4-vite] - 2022-04-27

### Added

- `useLoading` composable which can be used as a service to manage a global instance of `UiLoading` (a1aecbf275f1a786ce94abbc14df21f4adf785f0).

### Changed

- Added `isActive` prop to `UiLoading`, which is a wrapper around `v-if` and is used to show/hide the `UiLoading` component (f35524b71e4648b2cbae794b7e7a5896dab6d443).
- Changes to `isActive` or `withScrollLock` of `UiLoading` are now reactively handled to manage the scroll lock (f35524b71e4648b2cbae794b7e7a5896dab6d443).
- Updated `lint-staged`, `stylelint`, and `vite` to their latest versions.

## [0.25.3-vite] - 2022-04-26

### Fixed

- Broken types entry for lib build due to recent changes in `vite-plugin-dts` (2204605c42ca709dc9403b562cc22bd4e02ce3a4).

## [0.25.2-vite] - 2022-04-26

### Fixed

- Docs sidebar not displayed due to regressions in the recent version of `vite-plugin-md` (a48dd2c06b9051655951eb5fa76adbc0fd1fb6e7).
- Updated `@typescript-eslint/eslint-plugin` and `@typescript-eslint/parser` to their latest versions.

## [0.25.1-vite] - 2022-04-25

### Changed

- Improved type of `steps` prop of `UiLoading` (8018bc4e94334c61c1b39b88a0642110244d4ca6).
- Support additional statuses in `UiLoadingStep` (0954c2ef70b04cdef5392c133fb6b3bb4a5db5ea).
- Updated the `feedback` docs page (7dcfb7330e1b10095b4c0509cf6981e12e1b8e9f).
- Updated `autoprefixer`, `eslint`, `vite-plugin-dts`, `vite-plugin-md`, and `vue-tsc` to their latest versions.

## [0.25.0-vite] - 2022-04-22

### Added

- `UiLoading` component and its child component `UiLoadingStep`.
- `feedback` docs page which illustrates usage of `UiLoading`.

### Changed

- Improve screenreader accessibility of emoji icon in `UiCallOut` (#4).
- Use an irregular spin animation for the `loading` state of `UiButton`.
- Updated `nanoid`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vueuse/core`, `eslint-plugin-vue`, `lint-staged`, `markdown-it-prism`, `postcss-html`, `stylelint`, `tailwindcss`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, `vue`, and `vue-tsc` to their latest versions.

## [0.24.2-vite] - 2022-04-11

### Changed

- Updated peer dependency `@vueuse/core` to its latest version.

### Fixed

- Incomplete build script since `0.23.x-vite` (91a46910dad15f0daff3ee12c887b08a0e7b1e77).

## [0.24.1-vite] - 2022-04-06

### Fixed

- Incorrect component exported as `UiTransitionFade` in library (da8419a3e47086ef6a351e37243a4e3bba44f191).

## [0.24.0-vite] - 2022-04-06

### Added

- `gray` variant of `UiButton` (31b6ff19ac657f380eb6d37997562d09657df5ef).

### Changed

- Added a ring offset to the `UiButton` for keyboard focus (09a2abc2ef1ac1d3cf00064daa3c361f8f1c56a1).

### Fixed

- Color of custom components, e.g. `UiButton`, overridden in slotted content of `UiCallOut` (649019ab18862f85df8819d10a50ccc349e8fb2c) and `UiMessage` (049eba893394921d38e2a14390b5b2b69ebb8484).

## [0.23.2-vite] - 2022-04-05

### Fixed

- Peer dependencies not updated to currently used versions (36df6ea8e47b9230bd409fc1c3dc739be07f6a11).

## [0.23.1-vite] - 2022-04-05

### Fixed

- Broken build due to missing dir (c18c9248cfb98929aa29906b9346a7b011831643).

## [0.23.0-vite] - 2022-04-05

### Added

- `UiTransitionFade` component.

### Changed

- Removed unused transition in `UiPage` (ea2e5ad2b5b269e63911c4cec0147608f1649001).
- Updated `nanoid`, `@commitlint/cli`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `autoprefixer`, `eslint`, `lint-staged`, `markdown-it-anchor`, `postcss`, `postcss-nesting`, `prettier`, `stylelint`, `typescript`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, `vite-plugin-md`, and `vue-router` to their latest versions.

### Fixed

- Item props of `UiSidebarNavItem` not reactive (44411914225f5184133c8807eb5977d382e32002).
- `UiBox` missing in library entrypoint.

## [0.22.4-vite] - 2022-03-09

### Changed

- Updated `@tailwindcss/forms`, `nanoid`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `eslint`, `eslint-config-prettier`, `eslint-plugin-vue`, `lint-staged`, `markdown-it-prism`, `postcss`, `postcss-nesting`, `stylelint`, `tailwindcss`, `typescript`, `unplugin-vue-components`, `vite`, `vite-plugin-md`, `vue-eslint-parser`, `vue-router`, and `vue-tsc` to their latest versions.

### Fixed

- Prose color conflicts with colored variants of `UiCallOut` and `UiMessage` (#3).

## [0.22.3-vite] - 2022-02-14

### Changed

- Updated `@tailwindcss/typography`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `eslint`, `eslint-plugin-vue`, `lint-staged`, `stylelint`, `stylelint-config-standard`, `tailwindcss`, `unplugin-vue-components`, `vite`, `vite-plugin-md`, `vue`, and `vue-tsc` to their latest versions.

### Fixed

- `package.json` exposed in the docs build (0195af80733d8e15bc18471328310cb505db32df).

## [0.22.2-vite] - 2022-02-04

### Fixed

- Issues with imported prop interfaces in a few component examples (docs) (121df4dba36627c3e73610e958c003c75a8b999c).
- Unintended text reflow on toggle animation of the `UiContainer` sidebar (83236aa228129d56ab004eb0fb25d5e777156c9c).

## [0.22.1-vite] - 2022-02-03

### Fixed

- Broken prop types in a few component examples (docs) (db23d92b0cee0804e01be94d1d6fdab510a455cc).

## [0.22.0-vite] - 2022-02-03

### Changed

- Improve width of the `UiContainer`'s `main` element (#2).
- Updated `@tailwindcss/typography`, `@commitlint/cli`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `eslint`, `eslint-plugin-vue`, `lint-staged`, `postcss`, `stylelint`, `tailwindcss`, `typescript`, `unplugin-vue-components`, `vite`, `vite-plugin-svg-icons`, `vue`, `vue-eslint-parser`, and `vue-tsc` to their latest versions.

## [0.21.2-vite] - 2022-01-19

### Fixed

- Left-over console logs.

## [0.21.1-vite] - 2022-01-18

### Added

- Support `touched` and `setTouched()` from FormVueLate/Vee-Validate (#1).

### Changed

- Updated `nanoid`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `eslint`, `lint-staged`, `tailwindcss`, `unplugin-vue-components`, `vue`, and `vue-tsc` to their latest versions.

## [0.21.0-vite] - 2022-01-17

### Changed

- **BREAKING:** Upgraded Tailwind CSS to v3. Accordingly, `@tailwindcss/forms` and `@tailwindcss/typography` have been updated to their latest versions as well.
- Updated `nanoid`, `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vue/eslint-config-typescript`, `@vueuse/core`, `autoprefixer`, `eslint`, `eslint-plugin-vue`, `lint-staged`, `markdown-it-prism`, `postcss-nesting`, `stylelint`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, `vite-plugin-md`, `vite-plugin-svg-icons`, and `vue-tsc` to their latest versions.

## [0.20.2-vite] - 2021-12-14

### Fixed

- Regression in styling of content in `UiMessage` (42d66cae42c77fd41542645dd386a1c36867af29).

## [0.20.1-vite] - 2021-12-14

### Changed

- Updated `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vueuse/core`, `postcss`, `postcss-nesting`, `typescript`, `unplugin-vue-components`, `vite`, `vite-plugin-md`, and `vue` to their latest versions.

### Fixed

- `currentColor` not applied to list item markers in content of `UiMessage` (4372b8f914dbb0363c7b177999445c27a2a46a65).
- `currentColor` not applied to list item markers in content of `UiCallOut` (3b6583868386ff78292b5bf97f3fb26d81f2f75a).

## [0.20.0-vite] - 2021-12-07

### Added

- Close mobile menu on click outside (c5e6ae3fa6606e7bf187b1726c34689e8f51132e).

### Changed

- Updated `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `eslint`, `eslint-plugin-vue`, `prettier`, and `unplugin-vue-components` to their latest versions.

### Fixed

- Peer dependency `@vueuse/core` incorrectly bundled in library (4b7174447cd579093d3fa6f5803214840a938037).
- Build error due to use of `const enum` in `UiPasswordInput` (cafe4068a5b93d410176aac5ab4bd1739789d689).

## [0.19.0-vite] - 2021-12-01

### Added

- `addon` slot and `addonLabel` prop on `UiInput` (daf9e996334d3be5a564d8cd2f0872aedb2d6e1a).
- Language based toggle label on `UiPasswordInput` (5a364ebc5cc1e5031f5cb85df875498d54740c63).

### Changed

- Updated `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vue/eslint-config-typescript`, `postcss`, `postcss-html`, `prettier`, `vue`, and `vue-tsc` to their latest versions.

### Fixed

- Incorrect binding of `modelValue` on `UiInput` with `type="file"` (cc4da3d39293d8f9d0b80640eb1a5012614eb97b).

## [0.18.1-vite] - 2021-11-24

### Fixed

- `tabindex` of `UiRadio` (4f07de9e262e4c6e0a7ad685391b2237019e9e68).

## [0.18.0-vite] - 2021-11-24

### Added

- Support icons in `UiSidebarNavItem` (e5d5f0162e0e87c2c93415cbc2f09d57580e823f).

### Changed

- Updated `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vue/eslint-config-typescript`, `eslint`, `eslint-plugin-vue`, `lint-staged`, `postcss-nesting`, `stylelint`, `stylelint-config-standard`, `typescript`, `vite-plugin-dts`, `vue`, and `vue-tsc` to their latest versions.

### Fixed

- Instructions for PurgeCSS setup (docs).

## [0.17.0-vite] - 2021-11-12

### Added

- `noIcon` mode for `UiCallOut` (2d681705ddf59db5a621773864755309f78d1a3d).

### Changed

- Removed unused pages from previous `nuxt` version.
- Improved style of slotted content in `UiMessage` (d90a40b25bfdc221949310da1c6141add4140e79).
- Updated `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vue/eslint-config-typescript`, `eslint`, `eslint-plugin-vue`, `unplugin-vue-components`, `vite`, `vite-plugin-dts`, and `vue-tsc` to their latest versions.

### Fixed

- Tailwind CSS setup instructions on home page (docs).
- Type generated not disabled on docs build (d0d84c1cb8451559a35d335dc794468045d7e918).
- Border color of `UiRadio` when `:focus-within` is applied (bcfb66541feeccd40c4f811be95a7185c11144a8).
- Slotted text color in `UiMessage` (c67b37a553058c85c95186e49214f86a8f2b2e38).
- Favicon bundled with library (8be203377d7e1285ddc5bb7c60af36a8ea937907).

## [0.16.11-vite] - 2021-11-04

### Fixed

- The library's TypeScript types were not bundled correctly in the previous release.

## [0.16.10-vite] - 2021-11-04

### Added

- Generate and bundle TypeScript types with the library (8fa982a4fae64eb1cdaecdb53c0c6b57a9e7dccd).

### Changed

- Improved a few minor things of the docs (routing, instructions).
- Removed the unused dependencies `babel-loader` and `vue-loader`.

### Fixed

- Tailwind CSS setup example in `README.md`.

## [0.16.9-vite] - 2021-11-03

### Changed

- Improved a few minor things of the docs (routing, instructions).
- Updated `@commitlint/cli`, `@commitlint/config-conventional`, `@typescript-eslint/eslint-plugin`, `@typescript-eslint/parser`, `@vitejs/plugin-vue`, `@vue/eslint-config-typescript`, `autoprefixer`, `babel-loader`, `eslint`, `eslint-plugin-prettier`, `husky`, `lint-staged`, `postcss`, `postcss-html`, `postcss-nesting`, `stylelint`, `stylelint-config-prettier`, `stylelint-config-standard`, `tailwindcss`, `unplugin-vue-components`, `vite`, `vite-plugin-md`, `vue`, `vue-eslint-parser`, `vue-loader`, and `vue-tsc` to their latest versions.

### Fixed

- Checkmark animation in static build (d0cf00b8cdf90d6218c4e538ea1ab685f80c7dd9).

## [0.16.8-vite] - 2021-10-28

### Fixed

- Incomplete module exports definition for library.
- Moved dependencies required by the library from `devDependencies` to `dependencies`.
- Broken instructions in the `README.md`.

## [0.16.7-vite] - 2021-10-28

### Added

- `UiSidebarNav` component and its child component `UiSidebarNavItem`.

### Changed

- **BREAKING:** Library and docs migrated to Vue 3 with [Vite.js](https://vitejs.dev/). This and all subsequent versions marked with the `vite` suffix are incompatible with earlier versions tailored for use with Nuxt.js. The UI components have been migrated to the [Composition API](https://vuejs.org/guide/extras/composition-api-faq.html) of Vue 3 and are now TypeScript-based.

## [0.16.6] - 2021-10-07

### Changed

- Improved width of the `main` element of the `UiContainer` (19d1c51189e54f4e7b0dedd89c5f645f6a40d41a)
- Updated `@tailwindcss/forms`, `@commitlint/cli`, `@commitlint/config-conventional`, `core-js`, `lint-staged`, and `postcss` to their latest versions.

## [0.16.5] - 2021-10-06

### Fixed

- Invalid grid properties used in the `UiContainer` (497ed7c9b41e13440922635ae23e955f7d10f07e).

## [0.16.4] - 2021-09-28

### Fixed

- Binding of `$attrs` to the `UiButton` (64054aef138e4563985b51a25e6321117b329e77).

## [0.16.3] - 2021-09-28

### Fixed

- Scoped styles not applied to `UiButton` in SSR when a custom component such as `nuxt-link` is used as the button `tag` (35117c23fed4fb3d2ac1e3ed3128ef0ecfd31906).

## [0.16.2] - 2021-09-27

### Changed

- Animate toggling the sidebar of the `UiContainer` (459cb6c9ff5eb5ac4996ec807e66a1ac8d2518d4).
- Updated `core-js` and `postcss` to their latest versions.

## [0.16.1] - 2021-09-24

### Fixed

- Broken `UiContainer` spacing in `UiPage` (533fb68704aa498f547a5c033019e87c3fef9912).

## [0.16.0] - 2021-09-24

### Changed

- Override default `maxWidth.prose` of Tailwind CSS with the `maxWidth` configured for the `@tailwindcss/typography` plugin (0a7a050e4290f5eea562bfb6400485b65c050e1f).
- Support custom content layout in `UiPage` via its new `hasCustomContent` prop (1813666c4005bba4635e38f555b62c2761dfb6e4).
- Updated `core-js`, `eslint-plugin-prettier`, `husky`, `postcss`, and `prettier` to their latest versions.

## [0.15.2] - 2021-08-12

### Fixed

- Removed icon of `UiTextarea` in the contact form example (docs).

## [0.15.1] - 2021-08-12

### Changed

- Improved selector specificity of `UiFormHints` and `UiFormErrors` overrides in `UiInput`, `UiSelect`, `UiTextarea`, `UiRadioGroup`, and `UiCheckboxGroup`.
- Updated `nuxt` to its latest version.

## [0.15.0] - 2021-08-11

### Added

- Support error messages and hints in `UiSelect` (6319a1fb5bca3d933721f57e9a8146b7475072b0).
- Display currently deployed version of docs in its navbar.

### Changed

- Prevent layout shifting of navbar items (`UiNavbarListItem`, `UiLanguageSelect`) due to bold font when active state is triggered (cef4e654f80f084c3ed0f4f61c1de3ef61d6f269, 9719b49a689c891ee63d6f4114ac877981ce2fdf).
- Made various additional improvements to `UiSelect` (6319a1fb5bca3d933721f57e9a8146b7475072b0).
- Explicitly link input and label of `UiCheckbox` (f7b6938aba5c09e8ecfe234551dbc431063bcc12) and `UiRadio` (65d046dd375711288dbc98b63bc0e4fd0ae3b98b).
- Updated `core-js` to its latest version.

### Fixed

- Handle `blur` events in `UiCheckboxGroup` (033bbde3f422330823e7990940862b7e32b7780e) and `UiRadioGroup` (e130ea732e131400cde5228305324c42108746fb).
- Icon color for `UiInput` with errors (dc7354b58b398f1a932237031981fbabf17ba8ff).

## [0.14.0] - 2021-08-06

### Added

- `UiFormErrors` component.
- `UiFormHints` component.
- Support error messages in input components (`UiInput`, `UiTextarea`, `UiPasswordInput`, `UiCheckboxGroup`, `UiRadioGroup`) via `errors` prop.
- Support form hints in input components (`UiInput`, `UiTextarea`, `UiPasswordInput`, `UiCheckboxGroup`, `UiRadioGroup`) via `hints` prop.
- Support `prefers-reduced motion` in `UiTransitionExpand` (f96e91cfb2ae92a30e5fcfdcbb3d7ac3ed85be1f).
- Docker build setup for the docs (4ca2c16490cc545623cf8b9e9b39a97a972667a9, 853247a1a3cffabc7aed3b0eb3930dcc33803b58).
- Added Usage instructions to the docs home page (3eef284e8550921ce4847ed470c2ab3b4226ba24).

### Changed

- Update `UiTransitionExpand` to also animate opacity (172ec6bb1f1b981f3332ac5b9b93004251d78126).
- Improve layout and accessibility of `UiTextarea` (22317938533ab818c91f7fd9c97d1844f7cffa21).
- Made various additional improvements to `UiCheckboxGroup` (ebc075140839432eaa8c6a279a61b80cc1026d85).
- Made various additional improvements to `UiRadioGroup` (c61cb64b9ad6b78eac821838f472c8dca3dc01d8).
- Improve prop binding in `UiCheckbox` (d45980a6bb82171382454097ae2040c978cee38a).
- Updated `eslint` and `lint-staged` to their latest versions.

## [0.13.0] - 2021-07-30

### Added

- `UiCheckbox` component.
- `UiCheckboxGroup` component.

### Changed

- Updated `core-js` to its latest version.

## [0.12.0] - 2021-07-29

### Added

- `UiRadio` component.
- `UiRadioGroup` component.

## [0.11.2] - 2021-07-27

### Fixed

- Navbar list items hidden on screens >= md (36ec1a36e856c5ea51d0b2e1d81f6effdcc258e8).

## [0.11.1] - 2021-07-27

### Changed

- Updated Tailwind preset to use the new `colors.sky` instead of `colors.lightBlue` (786bc6204f0924bed41cef4d520263b236215331).

## [0.11.0] - 2021-07-27

### Added

- `tailwind.preset.js` for a more streamlined configuration of Tailwind CSS (10d9eba8d1598c2180a42f1eb3cf89f333894c60).

### Changed

- Enable `hyphens` by default for `@tailwindcss/typography` (1f0fd331d9e9f4a91f00d888fc9224b64c461f61).
- Updated `@commitlint/cli`, `@commitlint/config-conventional`, `@nuxtjs/tailwindcss`, `eslint`, `husky`, `lint-staged`, and `postcss` to their latest versions.

## [0.10.2] - 2021-06-30

### Changed

- Removed unnecessary classes from `UiContainer` and `UiPage`.

## [0.10.1] - 2021-06-30

### Changed

- Hide empty `sidebar` and `main` elements of `UiContainer` on narrow viewports `UiPage` (ca42356d88bcac4a1020460148fbeb2ed1e57d0d).
- Updated `core-js`, and `prettier` to their latest versions.

## [0.10.0] - 2021-06-25

### Added

- `UiBox` component.
- `Container` component.

### Changed

- Refactored `UiPage` to use the `UiBox` and `UiContainer` components (4240ddb0c02305e3339bf65fdc0fab4f4c07d9aa).
- Updated `@nuxtjs/tailwindcss`, `core-js`, `eslint`, and `postcss` to their latest versions.

## [0.9.0] - 2021-06-14

### Added

- Black and gray variants of `UiMessage` (165dfee22d6d52eabe8ba819e4522c13c77903d9).
- `info` variant of `UiButton`, `UiCallOut`, and `UiMessage` (cb1fca231aa4debcc45886994c0b6bddca34042f).
- Examples of `UiPage` with sidebar, new `UiButton` variants, and `UiMessage` (docs).

### Changed

- Use `v-if` to hide empty icon slots of `UiInput` (ed3071dad4b21b59cdfcf23aa4aac568f0ec08dc).
- Changed primary theme color to tailwind's `violet` (64c1081be72d1ded16d22a1591950911bf23119b).
- Adjusted padding and leading of input components (0c790b8a0965de88bb1eb0abc442474b05378f5a).
- Explicitly center text in `UiButton` (755df55102fc87e4aceae3d867aef0e7c1cc0be5).
- Updated `@tailwindcss/forms`, `@tailwindcss/typography`, `@nuxtjs/eslint-config`, `@nuxtjs/tailwindcss`, `core-js`, `eslint`, `postcss`, and `prettier` to their latest versions.

### Fixed

- Left padding of `UiButton` (c948650488bad94544ce80789858a1a68bb42697).
- Left padding of `UiPasswordInput` (c948650488bad94544ce80789858a1a68bb42697).
- Text color of nested content in `UiMessage` (cc7ae6d82057e592cf6fc6b66f3134d2d9eca885).
- `icon-only` variant padding of `UiButton` (1a15a79bc9f2bf03f5eaa37cd1cb5e5cfd79bb4d).

## [0.8.2] - 2021-05-14

### Changed

- Updated `@commitlint/cli`, `@commitlint/config-conventional`, `@nuxtjs/tailwindcss`, `core-js`, `eslint`, `lint-staged`, `nuxt`, `postcss`, and `prettier` to their latest versions.

### Fixed

- Text color of nested content in `UiCallOut` (74f0b774185a30d18f1f766bcf5475a31a6c3f88).

## [0.8.1] - 2021-05-10

### Changed

- Replace previous method of conditionally showing the sidebar in `UiPage` with `hasSidebar` prop (31af2a87aa38b2eb4dce30cd3484385bbf65c75c).

## [0.8.0] - 2021-05-07

### Added

- `UiCallOut` component.

### Changed

- Improved spacing (gap) between sidebar and main containers in `UiPage` (20df3b38091a974e35b90d54f6965cca5f1551f9).
- Updated `@nuxtjs/tailwindcss`, `core-js`, and `postcss` to their latest versions.

### Fixed

- Alignment of content in the `UiPage`'s sidebar (93c091d92f2b8f97a834ebbd05ee879d3f9440d1).

## [0.7.1] - 2021-05-05

### Fixed

- Conditional sidebar rendering in `UiPage` (635e9d79830b0f896a6506c622ce3cbd515cd048).

## [0.7.0] - 2021-05-04

### Added

- Added sidebar slot to `UiPage` (ec70b99f5588bc08729762d42773d8e8fed19911).

### Changed

- Increase max line width (`@tailwindcss/typography` configuration) (c201056bd2adb24dde7855cac0e725d50200dce2).
- Updated `@tailwindcss/forms` , `@commitlint/cli`, `@commitlint/config-conventional`, `@nuxtjs/tailwindcss`, `core-js`, `eslint`, `eslint-config-prettier`, `eslint-plugin-prettier`, `husky`, `nuxt`, `postcss`, `stylelint`, and `stylelint-config-standard` to their latest versions.

## [0.6.7] - 2021-03-12

### Changed

- Improved loading state of `UiButton` (da225e2a90f0bd49f8f69429ed5e835c1bc9d44e).
- Updated `@nuxtjs/tailwindcss` and `nuxt` to their latest versions.

### Fixed

- All icons affected by spinning animation of `UiButton` loading state (cadd4ec0009b14fec7917fce97261ecc9599b004).

## [0.6.6] - 2021-03-10

### Added

- Added loading state to `UiButton` (dbe859b6b6ee3be6d38995067e53d34b5a93db0c).

## [0.6.5] - 2021-03-08

### Fixed

- Padding of `UiInput` when `iconRight` slot is populated (38fe920c75a6fa11f767908c1dbedea012d62bb0).
- Toggle button of the `UiPasswordInput` covering the entered password (f65a51676bb8ac939473dd9184b10a282076a209).

### Changed

- Updated `@nuxtjs/eslint-config`, `stylelint`, and `stylelint-config-standard` to their latest versions.

## [0.6.4] - 2021-03-04

### Added

- Added `supportNoScript` prop to `UiNavbar`, which ensures usability of the navbar on mobile viewports in environments where JavaScript support is not guaranteed (56b9863418fa46778f5191731fffdac989928531).

### Changed

- Updated `@commitlint/cli`, `@commitlint/config-conventional`, `core-js`, `eslint`, and `eslint-config-prettier` to their latest versions.

## [0.6.3] - 2021-02-23

### Changed

- Reverted purging tailwind output in development.
- Updated `@commitlint/cli`, `@commitlint/config-conventional`, and `nuxt` to their latest versions.

## [0.6.2] - 2021-02-22

### Changed

- Purge tailwind output in development (in addition to production).
- Updated instructions for tailwind configuration.
- Updated `nuxt` and `stylelint` to their latest versions.

### Fixed

- `aria-checked` state of the `UiPasswordInput` (360cae60e7c659d09fc8b48a105864ce7f74658f).

## [0.6.1] - 2021-02-19

### Fixed

- Prevent `submit` from password toggle button in the `UiPasswordInput` (037a532837367a7be5e110efe0de473debb724ac).

## [0.6.0] - 2021-02-19

### Added

- `iconLeft` and `iconRight` slots to the `UiInput` (657b6fd63bd307cbadfe65470827c896abee24d4).
- `UiPasswordInput` component.

### Changed

- Updated `core-js` to its latest version.

## [0.5.0] - 2021-02-17

### Added

- `UiTransitionExpand` transition component.

### Changed

- Use inline SVG icons in `UiNavbarToggleButton` instead of relying on icons being present in the assets directory (61327c42b9b89a71cbd7555521aaf92a6f0b541e).
- Improved complexity and responsiveness of the `UiNavbar` (c818b9d40e1412e4558984e8328209792a0004f1).

### Fixed

- Bubbling of the language update event in `UiNavbar` (9fafde3a51951377c15f5c7dc77d955786a0839f).

## [0.4.0] - 2021-02-16

### Added

- Support disabling `prose` classes on `UiPage` via the `noProse` prop (46c6113fef700e6f9cbd16fdc3aa566e1d12495d).
- `UiBreadcrumbs` component (0519a2a9ab24d99664ae90efcde6aaf6bc1def75).

### Changed

- Updated docs: layout and content structure changes, along with additional and fixed examples.
- Removed `$i18n` dependency from `UiLanguageSelect` which uses props instead (02590d28bb79041fa7889cc2514696707559ac73).

### Fixed

- `v-model` handling in `UiSelect` (6fb3bb0c0a85a0ab96187cf01eaeef7a3bfb8203).

## [0.3.0] - 2021-02-15

### Added

- Sticky `UiNavbar` when setting its `sticky` prop to `true` (6bd902c2322fa043bfdbb6cb698fbd3cd797a7df).
- `UiPage` component (f1e0da8cbb60068377a03d5a47d7a93a24d4f598).

### Changed

- Updated `eslint`, `nuxt`, and `stylelint` to their latest versions.
- Updated `tailwindcss` to its latest compatibility version.

### Fixed

- Nested components not being resolved when using the NPM package (25026442b0fea6796912ba05ba58968855a95a2e).
- With JavaScript deactivated, the active style was applied to both the default language and the actual active language of the `UiLanguageSelect` component (1eb48e0c383141f34088830d2f4e8229000d6a2b).
- Added missing `@nuxtjs/svg-sprite` dependency and usage instructions (91270f248d9248b57918c7b2e004399c2bacf5f3).
- Added missing SVG icons used by the `UiNavbarToggleButton` (b3bdf48d38af0e1435793a9dca26c8fcbc66112c).
- Removed `$i18n` dependency from `UiNavbarToggleButton` (5f306541d80539e73d1719902802107c210dc0ec).

## [0.2.1] - 2021-02-12

### Fixed

- Passing props and listeners from `UiNavbar` to `UiNavbarListItem` (08ee0fb8e07de307f87d49e482af4cd6c3762b77).

## [0.2.0] - 2021-02-12

### Added

- `UiNavbar` component and child components `UiNavbarList`, `UiNavbarListItem`, `UiNavbarToggleButton`, and `UiLanguageSelect` (4619cccbf93e52da5ca03771fdae08e0e5f827ee).

### Changed

- Adjusted typography settings (0f8a1c48bba59f43afb356969213eac34e7e03d5).
- Reduced library size by moving various dependencies to `devDependencies` (9d26406c5463d6b2ec1939526c6ecba93220ca86).

### Fixed

- Instructions in the README.

## [0.1.1] - 2021-02-11

### Fixed

- Package name (changed to `@ise-hl/dsdcare-ui`) and publishing.

## [0.1.0] - 2021-02-11

### Added

- Initial application and library setup, including development environment, `tailwind` and `nuxt` setup, and build pipeline configuration.
- `UiButton` component.
- `UiInput` component.
- `UiMessage` component.
- `UiSelect` component.
- `UiTextarea` component.
- Docs with various examples of the UI components.

[unreleased]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.4.2...master
[1.4.2]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.4.1...v1.4.2
[1.4.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.4.0...v1.4.1
[1.4.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.3.0...v1.4.0
[1.3.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.2.2...v1.3.0
[1.2.2]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.2.1...v1.2.2
[1.2.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.2.0...v1.2.1
[1.2.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.1.1...v1.2.0
[1.1.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.0.4...v1.1.0
[1.0.4]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.0.3...v1.0.4
[1.0.3]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.0.2...v1.0.3
[1.0.2]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.32.1-vite...v1.0.0
[0.32.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.32.0-vite...v0.32.1-vite
[0.32.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.31.2-vite...v0.32.0-vite
[0.31.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.31.1-vite...v0.31.2-vite
[0.31.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.31.0-vite...v0.31.1-vite
[0.31.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.30.0-vite...v0.31.0-vite
[0.30.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.29.2-vite...v0.30.0-vite
[0.29.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.29.1-vite...v0.29.2-vite
[0.29.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.29.0-vite...v0.29.1-vite
[0.29.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.28.1-vite...v0.29.0-vite
[0.28.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.28.0-vite...v0.28.1-vite
[0.28.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.27.2-vite...v0.28.0-vite
[0.27.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.27.1-vite...v0.27.2-vite
[0.27.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.27.0-vite...v0.27.1-vite
[0.27.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.26.3-vite...v0.27.0-vite
[0.26.3-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.26.2-vite...v0.26.3-vite
[0.26.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.26.1-vite...v0.26.2-vite
[0.26.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.26.0-vite...v0.26.1-vite
[0.26.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.25.5-vite...v0.26.0-vite
[0.25.5-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.25.4-vite...v0.25.5-vite
[0.25.4-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.25.3-vite...v0.25.4-vite
[0.25.3-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.25.2-vite...v0.25.3-vite
[0.25.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.25.1-vite...v0.25.2-vite
[0.25.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.25.0-vite...v0.25.1-vite
[0.25.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.24.2-vite...v0.25.0-vite
[0.24.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.24.1-vite...v0.24.2-vite
[0.24.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.24.0-vite...v0.24.1-vite
[0.24.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.23.2-vite...v0.24.0-vite
[0.23.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.23.1-vite...v0.23.2-vite
[0.23.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.23.0-vite...v0.23.1-vite
[0.23.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.22.4-vite...v0.23.0-vite
[0.22.4-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.22.3-vite...v0.22.4-vite
[0.22.3-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.22.2-vite...v0.22.3-vite
[0.22.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.22.1-vite...v0.22.2-vite
[0.22.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.22.0-vite...v0.22.1-vite
[0.22.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.21.2-vite...v0.22.0-vite
[0.21.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.21.1-vite...v0.21.2-vite
[0.21.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.21.0-vite...v0.21.1-vite
[0.21.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.20.2-vite...v0.21.0-vite
[0.20.2-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.20.1-vite...v0.20.2-vite
[0.20.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.20.0-vite...v0.20.1-vite
[0.20.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.19.0-vite...v0.20.0-vite
[0.19.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.18.1-vite...v0.19.0-vite
[0.18.1-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.18.0-vite...v0.18.1-vite
[0.18.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.17.0-vite...v0.18.0-vite
[0.17.0-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.11-vite...v0.17.0-vite
[0.16.11-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.10-vite...v0.16.11-vite
[0.16.10-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.9-vite...v0.16.10-vite
[0.16.9-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.8-vite...v0.16.9-vite
[0.16.8-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.7-vite...v0.16.8-vite
[0.16.7-vite]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.6...v0.16.7-vite
[0.16.6]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.5...v0.16.6
[0.16.5]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.4...v0.16.5
[0.16.4]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.3...v0.16.4
[0.16.3]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.2...v0.16.3
[0.16.2]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.1...v0.16.2
[0.16.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.16.0...v0.16.1
[0.16.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.15.2...v0.16.0
[0.15.2]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.15.1...v0.15.2
[0.15.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.15.0...v0.15.1
[0.15.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.14.0...v0.15.0
[0.14.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.13.0...v0.14.0
[0.13.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.12.0...v0.13.0
[0.12.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.11.2...v0.12.0
[0.11.2]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.11.1...v0.11.2
[0.11.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.11.0...v0.11.1
[0.11.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.10.2...v0.11.0
[0.10.2]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.10.1...v0.10.2
[0.10.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.10.0...v0.10.1
[0.10.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.9.0...v0.10.0
[0.9.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.8.2...v0.9.0
[0.8.2]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.8.1...v0.8.2
[0.8.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.8.0...v0.8.1
[0.8.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.7.1...v0.8.0
[0.7.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.6.7...v0.7.0
[0.6.7]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.6.6...v0.6.7
[0.6.6]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.6.5...v0.6.6
[0.6.5]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.6.4...v0.6.5
[0.6.4]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.6.3...v0.6.4
[0.6.3]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.6.2...v0.6.3
[0.6.2]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.1.1...v0.2.0
[0.1.1]: https://gitlab.com/ise-hl/dsdcare/ui/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/ise-hl/dsdcare/ui/-/tags/v0.1.0
