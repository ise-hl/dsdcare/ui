import { PropType } from 'vue'

export function useInputProps() {
  return {
    disabled: {
      type: Boolean,
      default: false,
    },
    hints: {
      type: [String, Array] as PropType<string | string[]>,
      default: '',
    },
    label: {
      type: String,
      default: '',
    },
    name: {
      type: String,
      default: '',
    },
    type: {
      type: String,
      default: 'text',
    },
    required: {
      type: Boolean,
      default: false,
    },
    highlighted: {
      type: Boolean,
      default: false,
    },
    highlightedHint: {
      type: String,
      default: '',
    },
    modelValue: {
      type: [String, Object] as PropType<string | FileList>,
      default: undefined,
    },
    errors: {
      type: [String, Array, Boolean] as PropType<string | string[] | boolean>,
      default: false,
    },
    touched: {
      type: Boolean,
      default: undefined,
    },
    setTouched: {
      type: Function as PropType<(touched: boolean) => void>,
      default: undefined,
    },
  }
}

export default useInputProps
