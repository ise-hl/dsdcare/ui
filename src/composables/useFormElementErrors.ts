import { computed } from 'vue'

export function useFormElementErrors(props: {
  errors: string[] | string | boolean
  touched?: boolean
}) {
  const hasErrors = computed(() => {
    if (props.touched === false) {
      return false
    }

    return !!(Array.isArray(props.errors) ? props.errors.length : props.errors)
  })

  const parsedErrors = computed(() => {
    if (props.touched === false) {
      return []
    }

    if (Array.isArray(props.errors)) {
      return props.errors
    }

    if (props.errors && typeof props.errors === 'string') {
      return [props.errors]
    }

    return []
  })

  return {
    hasErrors,
    parsedErrors,
  }
}

export default useFormElementErrors
